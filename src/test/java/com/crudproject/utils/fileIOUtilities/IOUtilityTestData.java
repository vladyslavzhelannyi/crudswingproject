package com.crudproject.utils.fileIOUtilities;

import com.crudproject.models.Person;

import java.util.ArrayList;

public class IOUtilityTestData {
    public static final Person PERSON_1 = new Person(1, "A", "A", 23, "A");
    public static final Person PERSON_2 = new Person(2, "B", "B", 24, "B");

    public static final Person[] PEOPLE_ARRAY_1 = new Person[] {PERSON_1, PERSON_2};
    public static final Person[] PEOPLE_ARRAY_2 = new Person[] {PERSON_1};

    public static final ArrayList<Person> PEOPLE_LIST_1 = new ArrayList<>();
    public static final ArrayList<Person> PEOPLE_LIST_2 = new ArrayList<>();
    public static final ArrayList<Person> PEOPLE_LIST_3 = new ArrayList<>();

    static {
        PEOPLE_LIST_1.add(PERSON_1);
        PEOPLE_LIST_1.add(PERSON_2);

        PEOPLE_LIST_2.add(PERSON_1);
    }
}
