package com.crudproject.utils.fileIOUtilities;

import com.crudproject.models.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.crudproject.utils.fileIOUtilities.IOUtilityTestData.*;
import static com.crudproject.utils.fileIOUtilities.IOUtilityTestData.PEOPLE_LIST_2;

public class YamlIOUtilityTest {
    ObjectMapper yamlMapper = Mockito.mock(ObjectMapper.class);
    File file = Mockito.mock(File.class);

    YamlIOUtility cut = new YamlIOUtility(file, yamlMapper);

    static Arguments[] readFileTestArgs() {
        return new Arguments[] {
                Arguments.arguments(PEOPLE_ARRAY_1, PEOPLE_LIST_3),
                Arguments.arguments(PEOPLE_ARRAY_2, PEOPLE_LIST_3)
        };
    }

    @ParameterizedTest
    @MethodSource("readFileTestArgs")
    void readFileTest(Person[] arrayToReturn, ArrayList<Person> expected) throws IOException {
        Mockito.when(yamlMapper.readValue(file, Person[].class)).thenReturn(arrayToReturn);
        ArrayList<Person> actual = cut.readFile();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] writeFileTestArgs() {
        return new Arguments[] {
                Arguments.arguments(PEOPLE_LIST_1, 1),
                Arguments.arguments(PEOPLE_LIST_2, 1)
        };
    }

    @ParameterizedTest
    @MethodSource("writeFileTestArgs")
    void writeFileTest(ArrayList<Person> listToWrite, int times) throws IOException {
        cut.writeFile(listToWrite);
        Mockito.verify(yamlMapper, Mockito.times(times)).writeValue(file, listToWrite);
    }
}
