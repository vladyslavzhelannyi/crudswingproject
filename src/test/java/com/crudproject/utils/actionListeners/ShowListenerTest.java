package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jDialogs.ReadByIdDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.utils.supportingUtils.CrudValidation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import javax.swing.*;

import static com.crudproject.utils.actionListeners.ApplyListenerTestData.ACTION_EVENT1;

public class ShowListenerTest {
    private static final ReadByIdDialog readByIdDialog = Mockito.mock(ReadByIdDialog.class);
    private static final CrudValidation crudValidation = Mockito.mock(CrudValidation.class);
    private static final JTextField idField = Mockito.mock(JTextField.class);
    private static final TablePanel tablePanel = Mockito.mock(TablePanel.class);

    private static ShowListener cut = new ShowListener(readByIdDialog, crudValidation, tablePanel, idField);

    static Arguments[] actionPerformedTestArgs() {
        return new Arguments[] {
            Arguments.arguments("123456789", true, 123456789, false, new Person(123456789, "A", "A", 23, "A")),
            Arguments.arguments("erwr", false, 123456789, false, new Person(123456789, "A", "A", 23, "A")),
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs")
    void actionPerformedTest(String idStr, boolean isId, int id, boolean doesExist, Person showPerson) {
        Mockito.when(idField.getText()).thenReturn(idStr);
        Mockito.when(crudValidation.isStringId(idStr)).thenReturn(isId);
        Mockito.when(tablePanel.doesIdExist(id)).thenReturn(doesExist);
        Mockito.when(tablePanel.getPersonById(id)).thenReturn(showPerson);

        cut.actionPerformed(ACTION_EVENT1);
        cut.actionPerformed(ACTION_EVENT1);
    }
}
