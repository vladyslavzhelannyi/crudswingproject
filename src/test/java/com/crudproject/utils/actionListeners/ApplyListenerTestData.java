package com.crudproject.utils.actionListeners;

import java.awt.event.ActionEvent;

public class ApplyListenerTestData {

    public static final String EVENT1 = "Read data";

    public static final Object OBJECT1 = new Object();

    public static final ActionEvent ACTION_EVENT1 = new ActionEvent(OBJECT1, 0, EVENT1);

    public static final String EVENT2 = "Read data";

    public static final Object OBJECT2 = new Object();

    public static final ActionEvent ACTION_EVENT2 = new ActionEvent(OBJECT2, 0, EVENT2);
}
