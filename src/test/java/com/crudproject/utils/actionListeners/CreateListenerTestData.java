package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;

import java.awt.event.ActionEvent;

import static com.crudproject.constants.PersonConstants.NAME;
import static com.crudproject.constants.ValidationConstants.*;
import static com.crudproject.constants.PersonConstants.*;

public class CreateListenerTestData {
    public static final String EVENT = "Create person";

    public static final Object OBJECT = new Object();

    public static final ActionEvent ACTION_EVENT_1 = new ActionEvent(OBJECT, 0, EVENT);
    public static final String NAME_TEXT_1 = "Aa";
    public static final String SURNAME_TEXT_1 = "Aa";
    public static final String ageText1 = "43";
    public static final String cityText1 = "Aa";
    public static final String nameValidation1 = VALIDATED;
    public static final String surnameValidation1 = VALIDATED;
    public static final String ageValidation1 = VALIDATED;
    public static final String cityValidation1 = VALIDATED;
    public static final int showErrorTimes1 = 0;
    public static final String errorInfo1 = null;
    public static final Person newPerson1 = new Person(123456789, "Aa", "Aa", 43, "Aa");
    public static final int createPersonTimes1 = 1;
    public static final int clearFieldsTimes1 = 1;
    public static final int closeDialogTimes1 = 1;
    public static final int id1 = 123456789;
    public static final boolean doesExist1 = false;

    public static final ActionEvent ACTION_EVENT_2 = new ActionEvent(OBJECT, 0, EVENT);
    public static final String nameText2 = "34#";
    public static final String surnameText2 = "Aa3";
    public static final String ageText2 = "$32";
    public static final String cityText2 = "45%";
    public static final String nameValidation2 = INCORRECT_INPUT;
    public static final String surnameValidation2 = INCORRECT_INPUT;
    public static final String ageValidation2 = INCORRECT_INPUT;
    public static final String cityValidation2 = INCORRECT_INPUT;
    public static final int showErrorTimes2 = 1;
    public static final String errorInfo2 =  NAME + ": " + nameValidation2 + "\n" + SURNAME + ": " + surnameValidation2 +
            "\n" + AGE + ": " + ageValidation2 + "\n" + CITY + ": " + cityValidation2 + "\n";
    public static final Person newPerson2 = new Person(123456789, "Aa", "Aa", 43, "Aa");
    public static final int createPersonTimes2 = 0;
    public static final int clearFieldsTimes2 = 0;
    public static final int closeDialogTimes2 = 0;
    public static final int id2 = 123456789;
    public static final boolean doesExist2 = false;
}
