package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jDialogs.CreatePersonDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.utils.supportingUtils.IdGenerator;
import com.crudproject.wrappers.fillers.CreateDialogFiller;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static com.crudproject.utils.actionListeners.CreateListenerTestData.*;

public class CreateListenerTest {
    public static final CreatePersonDialog createPersonDialog = Mockito.mock(CreatePersonDialog.class);
    public static final CRUDService crudService = Mockito.mock(CRUDService.class);
    public static final CrudValidation crudValidation = Mockito.mock(CrudValidation.class);
    public static final IdGenerator idGenerator = Mockito.mock(IdGenerator.class);
    public static final CreateDialogFiller createDialogFiller = Mockito.mock(CreateDialogFiller.class);
    public static final TablePanel tablePanel = Mockito.mock(TablePanel.class);
    public static final JTextField nameField = Mockito.mock(JTextField.class);
    public static final JTextField surnameField = Mockito.mock(JTextField.class);
    public static final JTextField ageField = Mockito.mock(JTextField.class);
    public static final JTextField cityField = Mockito.mock(JTextField.class);

    public static CreateListener cut;

    static {
        Mockito.when(createDialogFiller.getNameField()).thenReturn(nameField);
        Mockito.when(createDialogFiller.getSurnameField()).thenReturn(surnameField);
        Mockito.when(createDialogFiller.getAgeField()).thenReturn(ageField);
        Mockito.when(createDialogFiller.getCityField()).thenReturn(cityField);
        cut = new CreateListener(createPersonDialog, crudService, crudValidation, idGenerator, tablePanel,
                createDialogFiller);
    }

    static Arguments[] actionPerformedTestArgs() {
        return new Arguments[] {
          Arguments.arguments(ACTION_EVENT_2, nameText2, surnameText2, ageText2, cityText2, nameValidation2,
                  surnameValidation2, ageValidation2, cityValidation2, showErrorTimes2, errorInfo2, newPerson2,
                  createPersonTimes2, clearFieldsTimes2, closeDialogTimes2, id2, doesExist2),
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs")
    void actionPerformedTest(ActionEvent event, String nameText, String surnameText, String ageText, String cityText,
                             String nameValidation, String surnameValidation, String ageValidation, String cityValidation,
                             int showErrorTimes, String errorInfo, Person newPerson, int createPersonTimes,
                             int clearFieldsTimes, int closeDialogTimes, int id, boolean doesExist) {
        Mockito.when(nameField.getText()).thenReturn(nameText);
        Mockito.when(surnameField.getText()).thenReturn(surnameText);
        Mockito.when(ageField.getText()).thenReturn(ageText);
        Mockito.when(cityField.getText()).thenReturn(cityText);
        Mockito.when(crudValidation.validateName(nameText)).thenReturn(nameValidation);
        Mockito.when(crudValidation.validateSurname(surnameText)).thenReturn(surnameValidation);
        Mockito.when(crudValidation.validateAge(ageText)).thenReturn(ageValidation);
        Mockito.when(crudValidation.validateCity(cityText)).thenReturn(cityValidation);
        Mockito.when(idGenerator.getNewId()).thenReturn(id);
        Mockito.when(tablePanel.doesIdExist(id)).thenReturn(doesExist);

        cut.actionPerformed(event);

        Mockito.verify(createPersonDialog, Mockito.times(showErrorTimes)).showErrorMessage(errorInfo);
        Mockito.verify(crudService, Mockito.times(createPersonTimes)).createPerson(newPerson);
        Mockito.verify(createPersonDialog, Mockito.times(clearFieldsTimes)).clearFields();
        Mockito.verify(createPersonDialog, Mockito.times(closeDialogTimes)).closeDialog();
    }

    static Arguments[] actionPerformedTestArgs1() {
        return new Arguments[] {
          Arguments.arguments(ACTION_EVENT_1, NAME_TEXT_1, SURNAME_TEXT_1, ageText1, cityText1, nameValidation1,
                surnameValidation1, ageValidation1, cityValidation1, showErrorTimes1, errorInfo1, newPerson1,
                createPersonTimes1, clearFieldsTimes1, closeDialogTimes1, id1, doesExist1),
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs1")
    void actionPerformedTest1(ActionEvent event, String nameText, String surnameText, String ageText, String cityText,
                             String nameValidation, String surnameValidation, String ageValidation, String cityValidation,
                             int showErrorTimes, String errorInfo, Person newPerson, int createPersonTimes,
                             int clearFieldsTimes, int closeDialogTimes, int id, boolean doesExist) {
        Mockito.when(nameField.getText()).thenReturn(nameText);
        Mockito.when(surnameField.getText()).thenReturn(surnameText);
        Mockito.when(ageField.getText()).thenReturn(ageText);
        Mockito.when(cityField.getText()).thenReturn(cityText);
        Mockito.when(crudValidation.validateName(nameText)).thenReturn(nameValidation);
        Mockito.when(crudValidation.validateSurname(surnameText)).thenReturn(surnameValidation);
        Mockito.when(crudValidation.validateAge(ageText)).thenReturn(ageValidation);
        Mockito.when(crudValidation.validateCity(cityText)).thenReturn(cityValidation);
        Mockito.when(idGenerator.getNewId()).thenReturn(id);
        Mockito.when(tablePanel.doesIdExist(id)).thenReturn(doesExist);

        cut.actionPerformed(event);

        Mockito.verify(createPersonDialog, Mockito.times(showErrorTimes)).showErrorMessage(errorInfo);
        Mockito.verify(crudService, Mockito.times(createPersonTimes)).createPerson(newPerson);
        Mockito.verify(createPersonDialog, Mockito.times(clearFieldsTimes)).clearFields();
        Mockito.verify(createPersonDialog, Mockito.times(closeDialogTimes)).closeDialog();
    }

    static Arguments[] checkIfInvokesAgainTestArgs() {
        return new Arguments[] {
          Arguments.arguments("Vl", "Vl", "23", "Vl", false),
          Arguments.arguments("", "", "", "", true)
        };
    }

    @ParameterizedTest
    @MethodSource("checkIfInvokesAgainTestArgs")
    void checkIfInvokesAgainTest(String field1, String field2, String field3, String field4, boolean expected) {
        boolean actual = cut.checkIfInvokesAgain(field1, field2, field3, field4);
        Assertions.assertEquals(expected, actual);
    }
}
