package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jDialogs.UpdatePersonDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.wrappers.fillers.UpdateDialogFiller;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static com.crudproject.utils.actionListeners.UpdateListenerTestData.*;

public class UpdateListenerTest {
    private static final CrudValidation crudValidation = Mockito.mock(CrudValidation.class);
    private static final CRUDService crudService = Mockito.mock(CRUDService.class);
    private static final UpdatePersonDialog updatePersonDialog = Mockito.mock(UpdatePersonDialog.class);
    private static final UpdateDialogFiller updateDialogFiller = Mockito.mock(UpdateDialogFiller.class);
    private static final JTextField nameField = Mockito.mock(JTextField.class);
    private static final JTextField surnameField = Mockito.mock(JTextField.class);
    private static final JTextField ageField = Mockito.mock(JTextField.class);
    private static final JTextField cityField = Mockito.mock(JTextField.class);
    private static final TablePanel tablePanel = Mockito.mock(TablePanel.class);

    private static UpdateListener cut;

    static {
        Mockito.when(updateDialogFiller.getNameField()).thenReturn(nameField);
        Mockito.when(updateDialogFiller.getSurnameField()).thenReturn(surnameField);
        Mockito.when(updateDialogFiller.getAgeField()).thenReturn(ageField);
        Mockito.when(updateDialogFiller.getCityField()).thenReturn(cityField);
        cut = new UpdateListener(crudValidation, crudService, updatePersonDialog, updateDialogFiller,
                tablePanel);
    }

    static Arguments[] actionPerformedTestArgs() {
        return new Arguments[] {
                Arguments.arguments(ACTION_EVENT_1, nameText1, surnameText1, ageText1, cityText1, nameValidation1,
                        surnameValidation1, ageValidation1, cityValidation1, showErrorTimes1, selectedPerson1,
                        updateTimes1, closeTimes1, rewrittenPerson1, errorInfo1)
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs")
    void actionPerformedTest(ActionEvent event, String nameText, String surnameText, String ageText, String cityText,
                             String nameValidation, String surnameValidation, String ageValidation, String cityValidation,
                             int showErrorTimes, Person selectedPerson, int updateTimes, int closeTimes,
                             Person rewrittenPerson, String errorInfo) {
        Mockito.when(nameField.getText()).thenReturn(nameText);
        Mockito.when(surnameField.getText()).thenReturn(surnameText);
        Mockito.when(ageField.getText()).thenReturn(ageText);
        Mockito.when(cityField.getText()).thenReturn(cityText);
        Mockito.when(crudValidation.validateName(nameText)).thenReturn(nameValidation);
        Mockito.when(crudValidation.validateSurname(surnameText)).thenReturn(surnameValidation);
        Mockito.when(crudValidation.validateAge(ageText)).thenReturn(ageValidation);
        Mockito.when(crudValidation.validateCity(cityText)).thenReturn(cityValidation);
        Mockito.when(tablePanel.getSelectedPerson()).thenReturn(selectedPerson);

        cut.actionPerformed(event);

        Mockito.verify(updatePersonDialog, Mockito.times(showErrorTimes)).showErrorMessage(errorInfo);
        Mockito.verify(crudService, Mockito.times(updateTimes)).updatePerson(rewrittenPerson);
        Mockito.verify(updatePersonDialog, Mockito.times(closeTimes)).closeDialog();
    }

    static Arguments[] actionPerformedTestArgs1() {
        return new Arguments[] {
                Arguments.arguments(ACTION_EVENT_2, nameText2, surnameText2, ageText2, cityText2, nameValidation2,
                        surnameValidation2, ageValidation2, cityValidation2, showErrorTimes2, selectedPerson2,
                        updateTimes2, closeTimes2, rewrittenPerson2, errorInfo2)
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs1")
    void actionPerformedTest1(ActionEvent event, String nameText, String surnameText, String ageText, String cityText,
                             String nameValidation, String surnameValidation, String ageValidation, String cityValidation,
                             int showErrorTimes, Person selectedPerson, int updateTimes, int closeTimes,
                             Person rewrittenPerson, String errorInfo) {
        Mockito.when(nameField.getText()).thenReturn(nameText);
        Mockito.when(surnameField.getText()).thenReturn(surnameText);
        Mockito.when(ageField.getText()).thenReturn(ageText);
        Mockito.when(cityField.getText()).thenReturn(cityText);
        Mockito.when(crudValidation.validateName(nameText)).thenReturn(nameValidation);
        Mockito.when(crudValidation.validateSurname(surnameText)).thenReturn(surnameValidation);
        Mockito.when(crudValidation.validateAge(ageText)).thenReturn(ageValidation);
        Mockito.when(crudValidation.validateCity(cityText)).thenReturn(cityValidation);
        Mockito.when(tablePanel.getSelectedPerson()).thenReturn(selectedPerson);

        cut.actionPerformed(event);

        Mockito.verify(updatePersonDialog, Mockito.times(showErrorTimes)).showErrorMessage(errorInfo);
        Mockito.verify(crudService, Mockito.times(updateTimes)).updatePerson(rewrittenPerson);
    }
}
