package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;

import java.awt.event.ActionEvent;

import static com.crudproject.constants.PersonConstants.*;
import static com.crudproject.constants.PersonConstants.CITY;
import static com.crudproject.constants.ValidationConstants.INCORRECT_INPUT;
import static com.crudproject.constants.ValidationConstants.VALIDATED;

public class UpdateListenerTestData {
    public static final String EVENT = "Update person";

    public static final Object OBJECT = new Object();

    public static final ActionEvent ACTION_EVENT_1 = new ActionEvent(OBJECT, 0, EVENT);
    public static final String nameText1 = "Aa";
    public static final String surnameText1 = "Aa";
    public static final String ageText1 = "43";
    public static final String cityText1 = "Aa";
    public static final String nameValidation1 = VALIDATED;
    public static final String surnameValidation1 = VALIDATED;
    public static final String ageValidation1 = VALIDATED;
    public static final String cityValidation1 = VALIDATED;
    public static final int showErrorTimes1 = 0;
    public static final Person selectedPerson1 = new Person(123456789, "Bb", "Bb", 23, "Bb");
    public static final int updateTimes1 = 1;
    public static final int closeTimes1 = 1;
    public static final Person rewrittenPerson1 = new Person(123456789, "Aa", "Aa", 43, "Aa");
    public static final String errorInfo1 =  null;

    public static final ActionEvent ACTION_EVENT_2 = new ActionEvent(OBJECT, 0, EVENT);
    public static final String nameText2 = "34#";
    public static final String surnameText2 = "Aa3";
    public static final String ageText2 = "$32";
    public static final String cityText2 = "45%";
    public static final String nameValidation2 = INCORRECT_INPUT;
    public static final String surnameValidation2 = INCORRECT_INPUT;
    public static final String ageValidation2 = INCORRECT_INPUT;
    public static final String cityValidation2 = INCORRECT_INPUT;
    public static final int showErrorTimes2 = 1;
    public static final Person selectedPerson2 = null;
    public static final int updateTimes2 = 0;
    public static final int closeTimes2 = 0;
    public static final Person rewrittenPerson2 = null;
    public static final String errorInfo2 =  NAME + ": " + nameValidation2 + "\n" + SURNAME + ": " + surnameValidation2 +
            "\n" + AGE + ": " + ageValidation2 + "\n" + CITY + ": " + cityValidation2 + "\n";
}
