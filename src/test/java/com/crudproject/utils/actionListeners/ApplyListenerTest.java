package com.crudproject.utils.actionListeners;

import com.crudproject.caches.DBEnum;
import com.crudproject.models.jElements.jDialogs.DBChoiceDialog;
import com.crudproject.services.CRUDService;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import static com.crudproject.constants.DBConstants.*;
import static com.crudproject.utils.actionListeners.ApplyListenerTestData.*;
import javax.swing.*;
import java.awt.event.ActionEvent;

public class ApplyListenerTest {
    private static final CRUDService crudService = Mockito.mock(CRUDService.class);
    private static final DBChoiceDialog dbChoiceDialog = Mockito.mock(DBChoiceDialog.class);
    private static final JComboBox<String> dbChoiceBox = Mockito.mock(JComboBox.class);

    private static final ApplyListener cut = new ApplyListener(crudService, dbChoiceDialog, dbChoiceBox);

    static Arguments[] actionPerformedTestArgs() {
        return new Arguments[] {
          Arguments.arguments(ACTION_EVENT1, GRAPH_DB_S, DBEnum.GRAPHDB, 1, 1),
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs")
    void actionPerformedTest(ActionEvent event, String dbChoice, DBEnum dbEnum, int setTableTimes,
                             int closeTimes) {
        Mockito.when(dbChoiceBox.getSelectedItem()).thenReturn((Object) dbChoice);

        cut.actionPerformed(event);

        Mockito.verify(crudService, Mockito.times(setTableTimes)).setTableData(dbEnum);
        Mockito.verify(dbChoiceDialog, Mockito.times(closeTimes)).closeDialog();
    }

    static Arguments[] actionPerformedTestArgs2() {
        return new Arguments[] {
          Arguments.arguments(ACTION_EVENT2, CASSANDRA_S, DBEnum.CASSANDRA, 1, 1),
          Arguments.arguments(ACTION_EVENT2, H2_S, DBEnum.H2, 1, 1),
          Arguments.arguments(ACTION_EVENT2, MONGO_DB_S, DBEnum.MONGODB, 1, 1),
          Arguments.arguments(ACTION_EVENT2, MY_SQL_S, DBEnum.MYSQL, 1, 1),
          Arguments.arguments(ACTION_EVENT2, POSTGRE_SQL_S, DBEnum.POSTGRESQL, 1, 1),
          Arguments.arguments(ACTION_EVENT2, REDIS_S, DBEnum.REDIS, 1, 1),
          Arguments.arguments(ACTION_EVENT2, BINARY_S, DBEnum.BINARY, 1, 1),
          Arguments.arguments(ACTION_EVENT2, CSV_S, DBEnum.CSV, 1, 1),
          Arguments.arguments(ACTION_EVENT2, JSON_S, DBEnum.JSON, 1, 1),
          Arguments.arguments(ACTION_EVENT2, XML_S, DBEnum.XML, 1, 1),
          Arguments.arguments(ACTION_EVENT2, YAML_S, DBEnum.YAML, 1, 1),
        };
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs2")
    void actionPerformedTest2(ActionEvent event, String dbChoice, DBEnum dbEnum, int setTableTimes,
                             int closeTimes) {
        Mockito.when(dbChoiceBox.getSelectedItem()).thenReturn((Object) dbChoice);

        cut.actionPerformed(event);

        Mockito.verify(crudService, Mockito.times(setTableTimes)).setTableData(dbEnum);
    }
}
