package com.crudproject.utils.factories;

import com.crudproject.caches.DBEnum;
import com.crudproject.utils.iDBUtilityImplementations.dbUtilities.*;
import com.crudproject.utils.iDBUtilityImplementations.fileUtilities.*;
import com.crudproject.utils.utilInterfaces.IDBUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

public class DBUtilsFactoryTest {
    private static final Map<DBEnum, IDBUtility> testDbMap = new HashMap<>();

    private static final CassandraUtility CASSANDRA_UTILITY = Mockito.mock(CassandraUtility.class);

    private static final GraphDBUtility GRAPH_DB_UTILITY = Mockito.mock(GraphDBUtility.class);

    private static final H2Utility H2_UTILITY = Mockito.mock(H2Utility.class);

    private static final MongoDBUtility MONGO_DB_UTILITY = Mockito.mock(MongoDBUtility.class);

    private static final MySqlUtility MY_SQL_UTILITY = Mockito.mock(MySqlUtility.class);

    private static final PostgreSqlUtility POSTGRE_SQL_UTILITY = Mockito.mock(PostgreSqlUtility.class);

    private static final RedisUtility REDIS_UTILITY = Mockito.mock(RedisUtility.class);

    private static final BinaryFileUtility BINARY_UTILITY = Mockito.mock(BinaryFileUtility.class);

    private static final CsvFileUtility CSV_FILE_UTILITY = Mockito.mock(CsvFileUtility.class);

    private static final JsonFileUtility JSON_FILE_UTILITY = Mockito.mock(JsonFileUtility.class);

    private static final XmlFileUtility XML_FILE_UTILITY = Mockito.mock(XmlFileUtility.class);

    private static final YamlFileUtility YAML_FILE_UTILITY = Mockito.mock(YamlFileUtility.class);

    static {
        testDbMap.put(DBEnum.CASSANDRA, CASSANDRA_UTILITY);
        testDbMap.put(DBEnum.GRAPHDB, GRAPH_DB_UTILITY);
        testDbMap.put(DBEnum.H2, H2_UTILITY);
        testDbMap.put(DBEnum.MONGODB, MONGO_DB_UTILITY);
        testDbMap.put(DBEnum.MYSQL, MY_SQL_UTILITY);
        testDbMap.put(DBEnum.POSTGRESQL, POSTGRE_SQL_UTILITY);
        testDbMap.put(DBEnum.REDIS, REDIS_UTILITY);
        testDbMap.put(DBEnum.BINARY, BINARY_UTILITY);
        testDbMap.put(DBEnum.CSV, CSV_FILE_UTILITY);
        testDbMap.put(DBEnum.JSON, JSON_FILE_UTILITY);
        testDbMap.put(DBEnum.XML, XML_FILE_UTILITY);
        testDbMap.put(DBEnum.YAML, YAML_FILE_UTILITY);
    }

    private static final DBUtilsFactory cut = new DBUtilsFactory(testDbMap);

    static Arguments[] getDBUtilTestArgs() {
        return new Arguments[] {
                Arguments.arguments(DBEnum.CASSANDRA, CASSANDRA_UTILITY),
                Arguments.arguments(DBEnum.GRAPHDB, GRAPH_DB_UTILITY),
                Arguments.arguments(DBEnum.H2, H2_UTILITY),
                Arguments.arguments(DBEnum.MONGODB, MONGO_DB_UTILITY),
                Arguments.arguments(DBEnum.MYSQL, MY_SQL_UTILITY),
                Arguments.arguments(DBEnum.POSTGRESQL, POSTGRE_SQL_UTILITY),
                Arguments.arguments(DBEnum.REDIS, REDIS_UTILITY),
                Arguments.arguments(DBEnum.BINARY, BINARY_UTILITY),
                Arguments.arguments(DBEnum.CSV, CSV_FILE_UTILITY),
                Arguments.arguments(DBEnum.JSON, JSON_FILE_UTILITY),
                Arguments.arguments(DBEnum.XML, XML_FILE_UTILITY),
                Arguments.arguments(DBEnum.YAML, YAML_FILE_UTILITY)
        };
    }

    @ParameterizedTest
    @MethodSource("getDBUtilTestArgs")
    void getDBUtilTest(DBEnum dbEnum, IDBUtility expected) {
        IDBUtility actual = cut.getDBUtil(dbEnum);
        Assertions.assertEquals(expected, actual);
    }
}
