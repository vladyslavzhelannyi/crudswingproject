package iDBUtilityImplementations.dbUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.iDBUtilityImplementations.dbUtilities.MySqlUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class H2UtilityTest {

    private static final String URL_H2 = "jdbc:h2:C:\\Users\\alena\\Desktop\\vladyslavzhelannyi-crudswingproject-eced5ccaa00e;MV_STORE=false";
    private static final String USER_H2 = "";
    private static final String PASSWORD_H2 = "";

    private static final String INSERT_SCRIPT = "INSERT INTO people VALUES (?, ?, ?, ?, ?);";
    private static final String READ_SCRIPT = "SELECT * FROM people;";
    private static final String UPDATE_SCRIPT = "UPDATE people SET name = ?, surname = ?, age = ?, city = ? WHERE id = ?;";
    private static final String DELETE_SCRIPT = "DELETE FROM people WHERE id = ?;";

    private final Connection connection = Mockito.mock(Connection.class);
    private final PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
    private final ResultSet resultSet = Mockito.mock(ResultSet.class);
    private final Person personTest = new Person(123456789, "Bill", "Brown", 20, "London");
    private final Person personTest1=new Person(123456789,"Bill","Brown",20,"London");

    private final MySqlUtility cut = new MySqlUtility();

    static List<Arguments> readDataTestArgs() {
        return List.of(
                Arguments.arguments(true, 1, 2, 1, 1, 1, 1, 1),
                Arguments.arguments(false, 1, 1, 0, 0, 0, 0, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("readDataTestArgs")
    void readDataTest(boolean next, int executeQueryTimes, int nextTimes, int idTimes, int fNameTimes, int lNameTimes,
                      int ageTimes, int cityTimes) {
        try (MockedStatic<DriverManager> mockDriverManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriverManager.when(() -> DriverManager.getConnection(URL_H2, USER_H2, PASSWORD_H2)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(READ_SCRIPT)).thenReturn(preparedStatement);
            Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
            Mockito.when(resultSet.next()).thenReturn(next).thenReturn(false);

            cut.readAll();

            Mockito.verify(preparedStatement, Mockito.times(executeQueryTimes)).executeQuery();
            Mockito.verify(resultSet, Mockito.times(nextTimes)).next();
            Mockito.verify(resultSet, Mockito.times(idTimes)).getInt("id");
            Mockito.verify(resultSet, Mockito.times(fNameTimes)).getString("fname");
            Mockito.verify(resultSet, Mockito.times(lNameTimes)).getString("lname");
            Mockito.verify(resultSet, Mockito.times(ageTimes)).getInt("age");
            Mockito.verify(resultSet, Mockito.times(cityTimes)).getString("city");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void createTest() {

        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(URL_H2, USER_H2, PASSWORD_H2)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(INSERT_SCRIPT)).thenReturn(preparedStatement);

            cut.add(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.getId());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.getName());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, personTest.getSurname());
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(4, personTest.getAge());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, personTest.getCity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(URL_H2, USER_H2, PASSWORD_H2)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(UPDATE_SCRIPT)).thenReturn(preparedStatement);

            cut.update(personTest);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(1, personTest.getName());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.getSurname());
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(3, personTest.getAge());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(4, personTest.getCity());
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(5, personTest.getId());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void deleteTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(URL_H2, USER_H2, PASSWORD_H2)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(UPDATE_SCRIPT)).thenReturn(preparedStatement);

            cut.delete(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.getId());
            Mockito.verify(preparedStatement, Mockito.times(1)).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}