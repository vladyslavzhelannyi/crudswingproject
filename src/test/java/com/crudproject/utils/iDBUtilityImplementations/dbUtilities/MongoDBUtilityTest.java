package com.crudproject.utils.iDBUtilityImplementations.dbUtilities;

import com.crudproject.models.Person;
import com.crudproject.wrappers.WrapperMongo;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Map;

import static com.crudproject.utils.iDBUtilityImplementations.dbUtilities.MongoTestData.*;

public class MongoDBUtilityTest {
    WrapperMongo wrapperMongo = Mockito.mock(WrapperMongo.class);
    MongoClient mongoClient = Mockito.mock(MongoClient.class);
    MongoDatabase database = Mockito.mock(MongoDatabase.class);
    MongoCollection<Document> mongoCollection = Mockito.mock(MongoCollection.class);
    UpdateResult updateResult = Mockito.mock(UpdateResult.class);
    DeleteResult deleteResult = Mockito.mock(DeleteResult.class);
    FindIterable<Document> iteratorDoc = Mockito.mock(FindIterable.class);
    MongoCursor it = Mockito.mock(MongoCursor.class);

    String nameDatabase = "crud";
    String nameCollection = "people";

    MongoDBUtility cut = new MongoDBUtility(wrapperMongo);

    static Arguments[] addTestArgs() {
        return new Arguments[] {
            Arguments.arguments(PERSON_1, 1, DOCUMENT_1),
            Arguments.arguments(PERSON_2, 1, DOCUMENT_2)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(Person person, int addTimes, Document document) {
        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);

        cut.add(person);

        Mockito.verify(mongoCollection, Mockito.times(addTimes)).insertOne(document);
    }

    static Arguments[] deleteTestArgs() {
        return new Arguments[] {
                Arguments.arguments(PERSON_1, 1, ID_DOC_1),
                Arguments.arguments(PERSON_2, 1, ID_DOC_2)
        };
    }

    @ParameterizedTest
    @MethodSource("deleteTestArgs")
    void deleteTest(Person person, int deleteTimes, Document document) {
        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);

        cut.delete(person);

        Mockito.verify(mongoCollection, Mockito.times(deleteTimes)).deleteOne(document);
    }

    static Arguments[] readAllTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, true, true, false, DOCUMENT_1, DOCUMENT_2, DOCUMENT_3,
                        TEST_LIST_1),
                Arguments.arguments(true, true, false, false, DOCUMENT_1, DOCUMENT_2, null,
                        TEST_LIST_2),
                Arguments.arguments(false, false, false, false, null, null, null,
                        new ArrayList<Person>())
        };
    }

    @ParameterizedTest
    @MethodSource("readAllTestArgs")
    void readAllTest(boolean hasNext1, boolean hasNext2, boolean hasNext3, boolean hasNext4, Document document1,
                     Document document2, Document document3, ArrayList<Person> expected){
        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);
        Mockito.when(mongoCollection.find()).thenReturn(iteratorDoc);
        Mockito.when(iteratorDoc.iterator()).thenReturn(it);
        Mockito.when(it.hasNext()).thenReturn(hasNext1).
                thenReturn(hasNext2).thenReturn(hasNext3).thenReturn(hasNext4);
        Mockito.when(it.next()).thenReturn(document1).thenReturn(document2).thenReturn(document3);

        ArrayList<Person> actual = cut.readAll();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] updateTestArgs() {
        return new Arguments[] {
            Arguments.arguments(PERSON_1, ID_DOC_1, new Document(Map.of("$set",
                    new Document(Map.of("name", PERSON_1.getName(), "surname", PERSON_1.getSurname(),
                            "age", PERSON_1.getAge(), "city", PERSON_1.getCity()))))),
            Arguments.arguments(PERSON_2, ID_DOC_2, new Document(Map.of("$set",
                    new Document(Map.of("name", PERSON_2.getName(), "surname", PERSON_2.getSurname(),
                            "age", PERSON_2.getAge(), "city", PERSON_2.getCity()))))),
        };
    }

    @ParameterizedTest
    @MethodSource("updateTestArgs")
    void updateTest(Person person, Document searchDoc, Document updateDocument) {
        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);
        Mockito.when(mongoCollection.updateOne(searchDoc, updateDocument)).thenReturn(updateResult);

        cut.update(person);

    }
}
