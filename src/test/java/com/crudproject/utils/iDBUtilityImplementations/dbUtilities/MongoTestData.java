package com.crudproject.utils.iDBUtilityImplementations.dbUtilities;

import com.crudproject.models.Person;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Map;

public class MongoTestData {



    public static ArrayList<Person> TEST_LIST_1 = new ArrayList<>();
    public static final ArrayList<Person> TEST_LIST_2 = new ArrayList<>();


    public static final Person PERSON_1 = new Person(123456789, "Vlad", "White", 10, "London");
    public static final Person PERSON_2 = new Person(223456789, "Bill", "Brown", 20, "Tokyo");
    public static final Person PERSON_3 = new Person(323456789, "Frank", "Black", 30, "Paris");

    public static final Document DOCUMENT_1 = new Document(Map.of("_id", PERSON_1.getId(), "name", PERSON_1.getName(),
            "surname", PERSON_1.getSurname(), "age", PERSON_1.getAge(), "city", PERSON_1.getCity()));
    public static final Document DOCUMENT_2 = new Document(Map.of("_id", PERSON_2.getId(), "name", PERSON_2.getName(),
            "surname", PERSON_2.getSurname(), "age", PERSON_2.getAge(), "city", PERSON_2.getCity()));
    public static final Document DOCUMENT_3 = new Document(Map.of("_id", PERSON_3.getId(), "name", PERSON_3.getName(),
            "surname", PERSON_3.getSurname(), "age", PERSON_3.getAge(), "city", PERSON_3.getCity()));

    public static final Document ID_DOC_1 = new Document(Map.of("_id", PERSON_1.getId()));
    public static final Document ID_DOC_2 = new Document(Map.of("_id", PERSON_2.getId()));
    public static final Document ID_DOC_3 = new Document(Map.of("_id", PERSON_3.getId()));

    static{
        TEST_LIST_1.add(PERSON_1);
        TEST_LIST_1.add(PERSON_2);
        TEST_LIST_1.add(PERSON_3);

        TEST_LIST_2.add(PERSON_1);
        TEST_LIST_2.add(PERSON_2);
    }


}
