package com.crudproject.utils.iDBUtilityImplementations.fileUtilities;

import com.crudproject.models.Person;

import java.util.ArrayList;

public class FileUtilityTestData {
    public static final Person PERSON_1 = new Person(1, "A", "A", 20, "A");
    public static final Person PERSON_2 = new Person(2, "B", "B", 21, "B");
    public static final Person PERSON_3 = new Person(3, "C", "C", 22, "C");
    public static final Person PERSON_4 = new Person(4, "D", "D", 23, "D");
    public static final Person PERSON_ADD = new Person(4, "Q", "Q", 24, "Q");
    public static final Person PERSON_DELETE_1 = new Person(1, "A", "A", 20, "A");
    public static final Person PERSON_DELETE_2 = new Person(3, "C", "C", 22, "C");
    public static final Person PERSON_UPDATE_1 = new Person(2, "X", "X", 25, "X");
    public static final Person PERSON_UPDATE_2 = new Person(4, "Y", "Y", 26, "Y");

    public static final ArrayList<Person> TEST_LIST_1 = new ArrayList<>();
    public static final ArrayList<Person> TEST_LIST_2 = new ArrayList<>();
    public static final ArrayList<Person> TEST_ADD_1 = new ArrayList<>();
    public static final ArrayList<Person> TEST_ADD_2 = new ArrayList<>();
    public static final ArrayList<Person> TEST_DELETE_1 = new ArrayList<>();
    public static final ArrayList<Person> TEST_DELETE_2 = new ArrayList<>();
    public static final ArrayList<Person> TEST_UPDATE_1 = new ArrayList<>();
    public static final ArrayList<Person> TEST_UPDATE_2 = new ArrayList<>();

    static {
        TEST_LIST_1.add(PERSON_1);
        TEST_LIST_1.add(PERSON_2);

        TEST_LIST_2.add(PERSON_3);
        TEST_LIST_2.add(PERSON_4);

        TEST_ADD_1.add(PERSON_1);
        TEST_ADD_1.add(PERSON_2);
        TEST_ADD_1.add(PERSON_ADD);

        TEST_ADD_2.add(PERSON_3);
        TEST_ADD_2.add(PERSON_4);
        TEST_ADD_2.add(PERSON_ADD);

        TEST_DELETE_1.add(PERSON_2);

        TEST_DELETE_2.add(PERSON_4);

        TEST_UPDATE_1.add(PERSON_1);
        TEST_UPDATE_1.add(PERSON_UPDATE_1);

        TEST_UPDATE_2.add(PERSON_3);
        TEST_UPDATE_2.add(PERSON_UPDATE_2);
    }
}
