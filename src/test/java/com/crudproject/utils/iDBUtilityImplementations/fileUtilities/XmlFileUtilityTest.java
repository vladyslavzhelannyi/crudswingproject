package com.crudproject.utils.iDBUtilityImplementations.fileUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.fileIOUtilities.XmlIOUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static com.crudproject.utils.iDBUtilityImplementations.fileUtilities.FileUtilityTestData.*;
import static com.crudproject.utils.iDBUtilityImplementations.fileUtilities.FileUtilityTestData.TEST_LIST_2;

public class XmlFileUtilityTest {
    XmlIOUtility xmlIOUtility = Mockito.mock(XmlIOUtility.class);

    XmlFileUtility cut = new XmlFileUtility(xmlIOUtility);

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(PERSON_ADD, new ArrayList<Person>(List.copyOf(TEST_LIST_1)), 1,
                        new ArrayList<Person>(List.copyOf(TEST_ADD_1))),
                Arguments.arguments(PERSON_ADD, new ArrayList<Person>(List.copyOf(TEST_LIST_2)), 1,
                        new ArrayList<Person>(List.copyOf(TEST_ADD_2))),
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(Person personToAdd, ArrayList<Person> people, int times, ArrayList<Person> peopleAfterAdd) {
        Mockito.when(xmlIOUtility.readFile()).thenReturn(people);
        cut.add(personToAdd);
        Mockito.verify(xmlIOUtility, Mockito.times(times)).writeFile(peopleAfterAdd);
    }

    static Arguments[] deleteTestArgs() {
        return new Arguments[] {
                Arguments.arguments(PERSON_DELETE_1, new ArrayList<Person>(List.copyOf(TEST_LIST_1)), 1,
                        new ArrayList<Person>(List.copyOf(TEST_DELETE_1))),
                Arguments.arguments(PERSON_DELETE_2, new ArrayList<Person>(List.copyOf(TEST_LIST_2)), 1,
                        new ArrayList<Person>(List.copyOf(TEST_DELETE_2))),
        };
    }

    @ParameterizedTest
    @MethodSource("deleteTestArgs")
    void deleteTest(Person personToDelete, ArrayList<Person> people, int times, ArrayList<Person> peopleAfterDelete) {
        Mockito.when(xmlIOUtility.readFile()).thenReturn(people);
        cut.delete(personToDelete);
        Mockito.verify(xmlIOUtility, Mockito.times(times)).writeFile(peopleAfterDelete);
    }

    static Arguments[] updateTestArgs() {
        return new Arguments[] {
                Arguments.arguments(PERSON_UPDATE_1, new ArrayList<Person>(List.copyOf(TEST_LIST_1)), 1,
                        new ArrayList<Person>(List.copyOf(TEST_UPDATE_1))),
                Arguments.arguments(PERSON_UPDATE_2, new ArrayList<Person>(List.copyOf(TEST_LIST_2)), 1,
                        new ArrayList<Person>(List.copyOf(TEST_UPDATE_2)))
        };
    }

    @ParameterizedTest
    @MethodSource("updateTestArgs")
    void updateTest(Person personToUpdate, ArrayList<Person> people, int times, ArrayList<Person> peopleAfterUpdate) {
        Mockito.when(xmlIOUtility.readFile()).thenReturn(people);
        cut.update(personToUpdate);
        Mockito.verify(xmlIOUtility, Mockito.times(times)).writeFile(peopleAfterUpdate);
    }

    static Arguments[] readAllTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new ArrayList<Person>(List.copyOf(TEST_LIST_1))),
                Arguments.arguments(new ArrayList<Person>(List.copyOf(TEST_LIST_2)))
        };
    }

    @ParameterizedTest
    @MethodSource("readAllTestArgs")
    void readAllTest(ArrayList<Person> expected) {
        Mockito.when(xmlIOUtility.readFile()).thenReturn(expected);
        ArrayList<Person> actual = cut.readAll();
        Assertions.assertEquals(expected, actual);
    }
}
