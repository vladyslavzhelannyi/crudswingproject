package com.crudproject.utils.supportingUtils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;

public class IdGeneratorTest {
    Random random = Mockito.mock(Random.class);
    IdGenerator cut = new IdGenerator(random);

    static Arguments[] getNewIdTestArgs() {
        return new Arguments[] {
            Arguments.arguments(1, 1, 0, 7, 6, 3, 5, 2, 3, 436478122),
            Arguments.arguments(2, 3, 1, 4, 4, 5, 2, 3, 0, 143655243)
        };
    }

    @ParameterizedTest
    @MethodSource("getNewIdTestArgs")
    void getNewIdTest(int int1, int int2, int int3, int int4, int int5, int int6, int int7, int int8, int int9,
                      int expected) {
        Mockito.when(random.nextInt(8)).thenReturn(int1).thenReturn(int2).thenReturn(int3).thenReturn(int4).
                thenReturn(int5).thenReturn(int6).thenReturn(int7).thenReturn(int8).thenReturn(int9);
        int actual = cut.getNewId();
        Assertions.assertEquals(expected, actual);
    }
}
