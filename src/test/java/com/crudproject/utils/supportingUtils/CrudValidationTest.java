package com.crudproject.utils.supportingUtils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CrudValidationTest {
    CrudValidation cut = new CrudValidation();

    static Arguments[] validateNameTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Vlad", "validated"),
                Arguments.arguments("Жан-Марк", "validated"),
                Arguments.arguments("San-Huan", "validated"),
                Arguments.arguments("FRANCHESKO", "Неверный ввод."),
                Arguments.arguments("|||@@@%%%", "Вы ввели некорректные символы.")
        };
    }

    @ParameterizedTest
    @MethodSource("validateNameTestArgs")
    void validateNameTest(String input, String expected){
        String actual = cut.validateName(input);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] validateSurnameTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Franc", "validated"),
                Arguments.arguments("Вупсень-Пупсень", "validated"),
                Arguments.arguments("San-pedro", "validated"),
                Arguments.arguments("GrEgOrIaN", "Неверный ввод."),
                Arguments.arguments("**//--++", "Вы ввели некорректные символы.")
        };
    }

    @ParameterizedTest
    @MethodSource("validateSurnameTestArgs")
    void validateSurnameTest(String input, String expected){
        String actual = cut.validateSurname(input);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] validateAgeTestArgs(){
        return new Arguments[]{
                Arguments.arguments("32", "validated"),
                Arguments.arguments("3221", "Слишком большое число"),
                Arguments.arguments("Number", "Вы ввели некорректные символы.")

        };
    }

    @ParameterizedTest
    @MethodSource("validateAgeTestArgs")
    void validateAgeTest(String input, String expected){
        String actual = cut.validateAge(input);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] validateCityTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Нью-Йорк", "validated"),
                Arguments.arguments("Copenhagen", "validated"),
                Arguments.arguments("Rio Grande", "validated"),
                Arguments.arguments("San-Francisco", "validated"),
                Arguments.arguments("Boobalehboobalehboobalehboobalehboobalehboobalehboobalehboobalehboobaleh" +
                                "Boobalehboobalehboobalehboobalehboobalehboobalehboobalehboobalehboobaleh" +
                                "Boobalehboobalehboobalehboobalehboobalehboobalehboobalehboobalehboobaleh" +
                                "Boobalehboobalehboobalehboobalehboobalehboobalehboobalehboobalehboobaleh" +
                                "Boobalehboobalehboobalehboobalehboobalehboobalehboobalehboobalehboobaleh",
                        "Слишком длинный ввод. Ограничение: 168 символов."),
                Arguments.arguments("PANDA", "Неверный ввод."),
                Arguments.arguments("/*-+", "Вы ввели некорректные символы."),
        };
    }

    @ParameterizedTest
    @MethodSource("validateCityTestArgs")
    void validateCityTest(String input, String expected){
        String actual = cut.validateCity(input);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] validateIdTestArgs(){
        return new Arguments[]{
                Arguments.arguments("32324235", false),
                Arguments.arguments("322142315", true),
                Arguments.arguments("idNumber", false)
        };
    }

    @ParameterizedTest
    @MethodSource("validateIdTestArgs")
    void validateIdTest(String input, boolean expected){
        boolean actual = cut.isStringId(input);
        Assertions.assertEquals(expected, actual);
    }

}
