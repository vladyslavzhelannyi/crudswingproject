package com.crudproject.wrappers.fillers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import java.lang.reflect.Field;

import static com.crudproject.constants.DBConstants.*;
import static com.crudproject.constants.DBConstants.YAML_S;

public class DBChoiceDialogFillerTest {
    JComboBox<String> dbChoiceBox = new JComboBox<>(new String[] {CASSANDRA_S, GRAPH_DB_S, H2_S, MONGO_DB_S,
            MY_SQL_S, POSTGRE_SQL_S, REDIS_S, BINARY_S, CSV_S, JSON_S, XML_S, YAML_S});
    JButton dbApplyButton = new JButton("Считать данные");

    DBChoiceDialogFiller cut = new DBChoiceDialogFiller();

    @Test
    void getDbChoiceBoxTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("dbChoiceBox");
        field1.setAccessible(true);
        field1.set(cut, dbChoiceBox);
        JComboBox<String> actual = cut.getDbChoiceBox();
        Assertions.assertEquals(dbChoiceBox, actual);
    }

    @Test
    void getDbApplyButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("dbApplyButton");
        field1.setAccessible(true);
        field1.set(cut, dbApplyButton);
        JButton actual = cut.getDbApplyButton();
        Assertions.assertEquals(dbApplyButton, actual);
    }
}
