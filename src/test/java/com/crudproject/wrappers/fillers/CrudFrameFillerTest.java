package com.crudproject.wrappers.fillers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.lang.reflect.Field;

import static com.crudproject.constants.AgeFilterConstants.*;

public class CrudFrameFillerTest {
    JButton readButton = new JButton("Read data");
    JButton updateButton = new JButton("Update record");
    JButton createButton = new JButton("Create record");
    JButton removeButton = new JButton("Remove record");
    JButton readIdButton = new JButton("Read by ID");
    JButton filterButton = new JButton("Filter by age");
    JComboBox<String> filterBox =
            new JComboBox<>(new String[] {NO_FILTER, FILTER018, FILTER1935, FILTER3655, FILTER56});

    CrudFrameFiller cut = new CrudFrameFiller();

    @Test
    void getReadButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("readButton");
        field1.setAccessible(true);
        field1.set(cut, readButton);
        JButton actual = cut.getReadButton();
        Assertions.assertEquals(readButton, actual);
    }

    @Test
    void getUpdateButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("updateButton");
        field1.setAccessible(true);
        field1.set(cut, updateButton);
        JButton actual = cut.getUpdateButton();
        Assertions.assertEquals(updateButton, actual);
    }

    @Test
    void getCreateButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("createButton");
        field1.setAccessible(true);
        field1.set(cut, createButton);
        JButton actual = cut.getCreateButton();
        Assertions.assertEquals(createButton, actual);
    }

    @Test
    void getRemoveButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("removeButton");
        field1.setAccessible(true);
        field1.set(cut, removeButton);
        JButton actual = cut.getRemoveButton();
        Assertions.assertEquals(removeButton, actual);
    }

    @Test
    void getReadIdButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("readIdButton");
        field1.setAccessible(true);
        field1.set(cut, readIdButton);
        JButton actual = cut.getReadIdButton();
        Assertions.assertEquals(readIdButton, actual);
    }

    @Test
    void getFilterButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("filterButton");
        field1.setAccessible(true);
        field1.set(cut, filterButton);
        JButton actual = cut.getFilterButton();
        Assertions.assertEquals(filterButton, actual);
    }

    @Test
    void getFilterBoxTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("filterBox");
        field1.setAccessible(true);
        field1.set(cut, filterBox);
        JComboBox<String> actual = cut.getFilterBox();
        Assertions.assertEquals(filterBox, actual);
    }
}
