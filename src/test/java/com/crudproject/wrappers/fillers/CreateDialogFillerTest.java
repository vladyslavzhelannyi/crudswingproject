package com.crudproject.wrappers.fillers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.lang.reflect.Field;

public class CreateDialogFillerTest {
    JTextField nameField = new JTextField();
    JTextField surnameField = new JTextField();
    JTextField ageField = new JTextField();
    JTextField cityField = new JTextField();
    JButton createButton = new JButton("Create person");

    CreateDialogFiller cut = new CreateDialogFiller();

    @Test
    void getNameFieldTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("nameField");
        field1.setAccessible(true);
        field1.set(cut, nameField);
        JTextField actual = cut.getNameField();
        Assertions.assertEquals(nameField, actual);
    }

    @Test
    void getSurnameFieldTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("surnameField");
        field1.setAccessible(true);
        field1.set(cut, surnameField);
        JTextField actual = cut.getSurnameField();
        Assertions.assertEquals(surnameField, actual);
    }

    @Test
    void getAgeFieldTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("ageField");
        field1.setAccessible(true);
        field1.set(cut, ageField);
        JTextField actual = cut.getAgeField();
        Assertions.assertEquals(ageField, actual);
    }

    @Test
    void getCityFieldTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("cityField");
        field1.setAccessible(true);
        field1.set(cut, cityField);
        JTextField actual = cut.getCityField();
        Assertions.assertEquals(cityField, actual);
    }

    @Test
    void getCreateButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("createButton");
        field1.setAccessible(true);
        field1.set(cut, createButton);
        JButton actual = cut.getCreateButton();
        Assertions.assertEquals(createButton, actual);
    }
}
