package com.crudproject.wrappers.fillers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.lang.reflect.Field;

public class ReadByIdDialogFillerTest {
    private JTextField idField = new JTextField();
    private JButton showButton = new JButton("Show record");

    ReadByIdDialogFiller cut = new ReadByIdDialogFiller();

    @Test
    void getIdFieldTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("idField");
        field1.setAccessible(true);
        field1.set(cut, idField);
        JTextField actual = cut.getIdField();
        Assertions.assertEquals(idField, actual);
    }

    @Test
    void getShowButtonTest() throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("showButton");
        field1.setAccessible(true);
        field1.set(cut, showButton);
        JButton actual = cut.getShowButton();
        Assertions.assertEquals(showButton, actual);
    }
}
