package com.crudproject.utils.iDBUtilityImplementations.dbUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IDBUtility;

import com.crudproject.wrappers.WrapperRedis;
import com.google.gson.Gson;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class RedisUtility implements IDBUtility {
    private WrapperRedis wrapperRedis;
    private Gson gson;

    public RedisUtility(WrapperRedis wrapperRedis, Gson gson) {
        this.wrapperRedis = wrapperRedis;
        this.gson = gson;
    }
    @Override
    public void add(Person person) {
        Jedis jedis = wrapperRedis.getJedis();
        String jsonPerson = gson.toJson(person);
        String key = Integer.toString(person.getId());

        jedis.append(key, jsonPerson);
        jedis.close();
    }

    @Override
    public void delete(Person person) {
        Jedis jedis = wrapperRedis.getJedis();

        String key = Integer.toString(person.getId());
        jedis.del(key);
        jedis.close();
    }

    @Override
    public ArrayList<Person> readAll() {
        Jedis jedis = wrapperRedis.getJedis();
        ArrayList<Person> personsList = new ArrayList<>();

        Set keys = jedis.keys("*");
        Iterator iterator = keys.iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String personJson = jedis.get(key);
            Person person = gson.fromJson(personJson, Person.class);
            personsList.add(person);
        }

        jedis.close();
        return personsList;
    }

    @Override
    public void update(Person person) {
        Jedis jedis = wrapperRedis.getJedis();

        String key = Integer.toString(person.getId());
        jedis.del(key);
        String jsonUpdatePerson = gson.toJson(person);
        jedis.append(key, jsonUpdatePerson);
        jedis.close();

    }
}
