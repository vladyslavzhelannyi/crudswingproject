package com.crudproject.utils.iDBUtilityImplementations.dbUtilities;

import com.crudproject.configs.JdbcConnectionConfig;
import com.crudproject.exceptions.FailedConnectionException;
import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IDBUtility;

import java.sql.*;
import java.util.ArrayList;

public class GraphDBUtility implements IDBUtility {


    private static final String INSERT_SCRIPT = "CREATE (n:People {id: ?, first_name: ?, last_name: ?, age: ?," +
            " city: ?}) RETURN n";
    private static final String READ_SCRIPT = "MATCH (n:People) RETURN n.id, n.first_name," +
            " n.last_name, n.age, n.city";
    private static final String UPDATE_SCRIPT = "MATCH (n:People) WHERE n.id=? SET n.first_name=?" +
            " SET n.last_name=? SET n.age=? SET n.city=? RETURN n";
    private static final String DELETE_SCRIPT = "MATCH (n:People) WHERE n.id=? DELETE n";
    private static final String PERSONS_ID_SCRIPT = "MATCH (n:People) RETURN n.id";

    @Override
    public void add(Person person) {

        try (Connection connection = JdbcConnectionConfig.getConnectionToNeo4j()) {

            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SCRIPT);

            preparedStatement.setInt(1, person.getId());
            preparedStatement.setString(2, person.getName());
            preparedStatement.setString(3, person.getSurname());
            preparedStatement.setInt(4, person.getAge());
            preparedStatement.setString(5, person.getCity());

            preparedStatement.execute();

            preparedStatement.close();

        } catch (SQLException | FailedConnectionException ex) {

            ex.printStackTrace();

        }

    }

    @Override
    public ArrayList<Person> readAll(){
        ArrayList<Person> people = new ArrayList<>();

        try (Connection connection = JdbcConnectionConfig.getConnectionToNeo4j()) {

            Statement stmt= connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(READ_SCRIPT);

            while(resultSet.next()) {
                Person newPerson = new Person();

                int id=resultSet.getInt("n.id");
                String name = resultSet.getString("n.first_name");
                String surname = resultSet.getString("n.last_name");
                int age = resultSet.getInt("n.age");
                String city = resultSet.getString("n.city");

                newPerson.setId(id);
                newPerson.setName(name);
                newPerson.setSurname(surname);
                newPerson.setAge(age);
                newPerson.setCity(city);

                people.add(newPerson);
            }
        } catch (SQLException | FailedConnectionException ex) {
            ex.printStackTrace();
        }
        return people;
    }

    @Override
    public void update(Person person) {

        try (Connection connection = JdbcConnectionConfig.getConnectionToNeo4j()) {

            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SCRIPT);

            preparedStatement.setInt(1, person.getId());
            preparedStatement.setString(2, person.getName());
            preparedStatement.setString(3, person.getSurname());
            preparedStatement.setInt(4, person.getAge());
            preparedStatement.setString(5, person.getCity());

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } catch (SQLException | FailedConnectionException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(Person person) {

        try (Connection connection = JdbcConnectionConfig.getConnectionToNeo4j()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SCRIPT);
            preparedStatement.setInt(1, person.getId());

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (SQLException | FailedConnectionException ex) {
            ex.printStackTrace();
        }
    }
}
