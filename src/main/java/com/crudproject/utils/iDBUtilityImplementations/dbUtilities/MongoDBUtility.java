package com.crudproject.utils.iDBUtilityImplementations.dbUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IDBUtility;
import com.crudproject.wrappers.WrapperMongo;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class MongoDBUtility implements IDBUtility {
    private WrapperMongo wrapperMongo;
    private MongoDatabase database;
    private MongoCollection<Document> mongoCollection;

    private static final String NAME_DATABASE = "crud";
    private static final String NAME_COLLECTION = "people";

    public MongoDBUtility(WrapperMongo wrapperMongo) {
        this.wrapperMongo = wrapperMongo;
    }

    @Override
    public void add(Person person) {
        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);

            Document document = new Document(Map.of("_id", person.getId(), "name", person.getName(),
                    "surname", person.getSurname(), "age", person.getAge(), "city", person.getCity()));

            mongoCollection.insertOne(document);
        }
    }

    @Override
    public void delete(Person person) {
        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);

            mongoCollection.deleteOne(new Document("_id", person.getId()));
        }
    }

    @Override
    public ArrayList<Person> readAll() {
        ArrayList<Person> personList;
        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);
            Document readDocument;
            Person readPerson;

            FindIterable<Document> iteratorDoc = mongoCollection.find();
            Iterator it = iteratorDoc.iterator();

            personList = new ArrayList<>();

            while (it.hasNext()) {
                readDocument = (Document) it.next();
                readPerson = new Person();

                readPerson.setId(Integer.parseInt(readDocument.get("_id").toString()));
                readPerson.setName(readDocument.get("name").toString());
                readPerson.setSurname((readDocument.get("surname").toString()));
                readPerson.setAge(Integer.parseInt(readDocument.get("age").toString()));
                readPerson.setCity(readDocument.get("city").toString());
                personList.add(readPerson);
            }
        }
        return personList;
    }

    @Override
    public void update(Person person) {
        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);

            Document searchDoc = new Document("_id", person.getId());

            mongoCollection.updateOne(searchDoc, new Document(Map.of("$set",
                    new Document(Map.of("name", person.getName(), "surname", person.getSurname(),
                    "age", person.getAge(), "city", person.getCity())))));
        }
    }
}
