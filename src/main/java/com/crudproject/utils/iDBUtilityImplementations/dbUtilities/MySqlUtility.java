package com.crudproject.utils.iDBUtilityImplementations.dbUtilities;

import com.crudproject.configs.JdbcConnectionConfig;
import com.crudproject.exceptions.FailedConnectionException;
import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IDBUtility;

import java.sql.*;
import java.util.ArrayList;

public class MySqlUtility implements IDBUtility {
    private static final String TABLE_CONFIG = "CREATE TABLE IF NOT EXISTS people (" +
            "id int8 NOT NULL PRIMARY KEY, " +
            "name CHARACTER VARYING(1500), " +
            "surname CHARACTER VARYING(1500) NOT NULL, " +
            "age int8 NOT NULL, city CHARACTER VARYING(168) NOT NULL," +
            "CONSTRAINT people_age_check CHECK(age >0 AND age < 200)" +
            ");";
    private static final String INSERT_SCRIPT = "INSERT INTO people VALUES (?, ?, ?, ?, ?);";
    private static final String DELETE_SCRIPT = "DELETE FROM people WHERE id = ?;";
    private static final String UPDATE_SCRIPT = "UPDATE people SET name = ?, surname = ?, age = ?, city = ? WHERE id = ?;";
    private static final String READ_SCRIPT = "SELECT * FROM people;";

    public MySqlUtility() {
        createTable();
    }

    @Override
    public void add(Person person) {
        try(
                Connection connection = JdbcConnectionConfig.getConnectionToMySql();
                PreparedStatement ps = connection.prepareStatement(INSERT_SCRIPT)
        ) {
            ps.setInt(1, person.getId());
            ps.setString(2, person.getName());
            ps.setString(3, person.getSurname());
            ps.setInt(4, person.getAge());
            ps.setString(5, person.getCity());
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Person person) {
        try(
                Connection connection = JdbcConnectionConfig.getConnectionToMySql();
                PreparedStatement ps = connection.prepareStatement(DELETE_SCRIPT)
        ) {
            ps.setInt(1, person.getId());
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Person> readAll() {
        ArrayList<Person> people = new ArrayList<>();
        try(
                Connection connection = JdbcConnectionConfig.getConnectionToMySql();
                PreparedStatement ps = connection.prepareStatement(READ_SCRIPT)
        ) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Person newPerson = new Person();

                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                int age = resultSet.getInt("age");
                String city = resultSet.getString("city");

                newPerson.setId(id);
                newPerson.setName(name);
                newPerson.setSurname(surname);
                newPerson.setAge(age);
                newPerson.setCity(city);

                people.add(newPerson);
            }
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }

        return people;
    }

    @Override
    public void update(Person person) {
        try(
                Connection connection = JdbcConnectionConfig.getConnectionToMySql();
                PreparedStatement ps = connection.prepareStatement(UPDATE_SCRIPT)
        ) {
            ps.setString(1, person.getName());
            ps.setString(2, person.getSurname());
            ps.setInt(3, person.getAge());
            ps.setString(4, person.getCity());
            ps.setInt(5, person.getId());
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    private void createTable() {
        try(
                Connection connection = JdbcConnectionConfig.getConnectionToMySql();
                Statement statement = connection.createStatement();
        ) {
            statement.executeUpdate(TABLE_CONFIG);
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }
}
