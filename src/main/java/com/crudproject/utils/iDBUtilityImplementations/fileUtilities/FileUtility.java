package com.crudproject.utils.iDBUtilityImplementations.fileUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IDBUtility;
import com.crudproject.utils.utilInterfaces.IFileIOUtility;

import java.util.ArrayList;
import java.util.List;

public abstract class FileUtility implements IDBUtility {
    private IFileIOUtility iFileIOUtility;

    protected void setIFileIOUtility(IFileIOUtility iFileIOUtility){
        this.iFileIOUtility = iFileIOUtility;
    }

    @Override
    public void add(Person person) {
        ArrayList<Person> people = iFileIOUtility.readFile();
        people.add(person);
        iFileIOUtility.writeFile(people);
    }

    @Override
    public void delete(Person person) {
        ArrayList<Person> people = iFileIOUtility.readFile();
        people.remove(person);
        iFileIOUtility.writeFile(people);
    }

    @Override
    public ArrayList<Person> readAll() {
        ArrayList<Person> people = iFileIOUtility.readFile();
        return people;
    }

    @Override
    public void update(Person person) {
        ArrayList<Person> people = iFileIOUtility.readFile();
        for (Person next : people) {
            if (next.getId() == person.getId()) {
                people.remove(next);
                people.add(person);
                break;
            }
        }
        iFileIOUtility.writeFile(people);
    }
}
