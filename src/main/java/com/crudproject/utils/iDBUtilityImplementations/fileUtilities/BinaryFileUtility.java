package com.crudproject.utils.iDBUtilityImplementations.fileUtilities;

import com.crudproject.utils.fileIOUtilities.BinaryIOUtility;

public class BinaryFileUtility extends FileUtility {

    public BinaryFileUtility(BinaryIOUtility binaryIOUtility) {
        setIFileIOUtility(binaryIOUtility);
    }
}