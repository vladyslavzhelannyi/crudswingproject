package com.crudproject.utils.itemListeners;

import com.crudproject.models.jElements.jPanels.TablePanel;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;

import static com.crudproject.constants.AgeFilterConstants.*;

public class FilterListener implements ItemListener {
    private Map<String, RowFilter<Object, Object>> filterMap;
    private TablePanel tablePanel;

    public FilterListener(Map<String, RowFilter<Object, Object>> filterMap, TablePanel tablePanel) {
        this.filterMap = filterMap;
        this.tablePanel = tablePanel;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            String item = (String) e.getItem();
            RowFilter<Object, Object> filter = null;
            switch (item) {
                case NO_FILTER:
                    filter = filterMap.get(NO_FILTER);
                    break;
                case FILTER018:
                    filter = filterMap.get(FILTER018);
                    break;
                case FILTER1935:
                    filter = filterMap.get(FILTER1935);
                    break;
                case FILTER3655:
                    filter = filterMap.get(FILTER3655);
                    break;
                case FILTER56:
                    filter = filterMap.get(FILTER56);
                    break;
            }
            tablePanel.filerTable(filter);
        }
    }
}