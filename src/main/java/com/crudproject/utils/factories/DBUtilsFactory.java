package com.crudproject.utils.factories;

import com.crudproject.caches.DBEnum;
import com.crudproject.utils.utilInterfaces.IDBUtility;

import java.util.Map;

public class DBUtilsFactory {
    private Map<DBEnum, IDBUtility> dbMap;

    public DBUtilsFactory(Map<DBEnum, IDBUtility> dbMap) {
        this.dbMap = dbMap;
    }

    public IDBUtility getDBUtil(DBEnum dbEnum) {
        switch (dbEnum) {
            case CASSANDRA:
                return dbMap.get(DBEnum.CASSANDRA);
            case GRAPHDB:
                return dbMap.get(DBEnum.GRAPHDB);
            case H2:
                return dbMap.get(DBEnum.H2);
            case MONGODB:
                return dbMap.get(DBEnum.MONGODB);
            case MYSQL:
                return dbMap.get(DBEnum.MYSQL);
            case POSTGRESQL:
                return dbMap.get(DBEnum.POSTGRESQL);
            case REDIS:
                return dbMap.get(DBEnum.REDIS);
            case BINARY:
                return dbMap.get(DBEnum.BINARY);
            case CSV:
                return dbMap.get(DBEnum.CSV);
            case JSON:
                return dbMap.get(DBEnum.JSON);
            case XML:
                return dbMap.get(DBEnum.XML);
            case YAML:
                return dbMap.get(DBEnum.YAML);
            default:
                return null;
        }
    }
}
