package com.crudproject.utils.fileIOUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.iDBUtilityImplementations.fileUtilities.FileUtility;
import com.crudproject.utils.utilInterfaces.IFileIOUtility;
import com.crudproject.wrappers.ObjectInputStreamWrapper;
import com.crudproject.wrappers.ObjectOutputStreamWrapper;

import java.io.*;
import java.util.ArrayList;

public class BinaryIOUtility implements IFileIOUtility {
    private File file;
    private ObjectInputStreamWrapper objectInputStreamWrapper;
    private ObjectOutputStreamWrapper objectOutputStreamWrapper;

    public BinaryIOUtility(File file, ObjectInputStreamWrapper objectInputStreamWrapper,
                           ObjectOutputStreamWrapper objectOutputStreamWrapper) {
        this.file = file;
        this.objectInputStreamWrapper = objectInputStreamWrapper;
        this.objectOutputStreamWrapper = objectOutputStreamWrapper;
    }

    @Override
    public ArrayList<Person> readFile() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }
        else {
            ArrayList<Person> newPeople = new ArrayList<>();
            try(ObjectInputStream ois = objectInputStreamWrapper.getObjectInputStream(file))
            {
                newPeople = ((ArrayList<Person>) ois.readObject());
            }
            catch(Exception e){
                e.printStackTrace();
            }
            return newPeople;
        }
    }

    @Override
    public void writeFile(ArrayList<Person> personArrayList) {
        try(ObjectOutputStream oos = objectOutputStreamWrapper.getObjectOutputStream(file))
        {
            oos.writeObject(personArrayList);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}