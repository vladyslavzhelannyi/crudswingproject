package com.crudproject.utils.fileIOUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IFileIOUtility;
import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvIOUtility implements IFileIOUtility {
    private File file;

    public CsvIOUtility(File file) {
        this.file = file;
    }

    @Override
    public ArrayList<Person> readFile() {
        if(file.length() == 0){
            return new ArrayList<Person>();
        }

        List<Person> result = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            HeaderColumnNameMappingStrategy<Person> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Person.class);

            CsvToBean<Person> csvToBean = new CsvToBeanBuilder<Person>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            result = csvToBean.parse();
        }
        catch(IOException e){
            System.out.println("Problem with read in csv ");
            e.printStackTrace();
        }

        ArrayList<Person> output = new ArrayList<>(result);
        return output;
    }

    @Override
    public void writeFile(ArrayList<Person> personArrayList) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

            StatefulBeanToCsv<Person> beanToCsv = new StatefulBeanToCsvBuilder<Person>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(personArrayList);
        }catch(CsvDataTypeMismatchException | CsvRequiredFieldEmptyException |
                IOException e){
            System.out.println("Problem with write in csv ");
            e.printStackTrace();
        }
    }
}