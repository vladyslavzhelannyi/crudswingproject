package com.crudproject.utils.fileIOUtilities;

import com.crudproject.models.Person;
import com.crudproject.utils.utilInterfaces.IFileIOUtility;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlIOUtility implements IFileIOUtility {
    private XmlMapper mapper;
    private File file;

    public XmlIOUtility(File file, XmlMapper mapper) {
        this.file = file;
        this.mapper = mapper;
    }

    @Override
    public ArrayList<Person> readFile() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }
        ArrayList<Person> newPeople =  null;
        Person[] persons = null;
        try {
            persons = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        newPeople = new ArrayList<Person>(List.of(persons));
        return newPeople;
    }

    @Override
    public void writeFile(ArrayList<Person> personArrayList) {
        try {
            mapper.writeValue(file, personArrayList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}