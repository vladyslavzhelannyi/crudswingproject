package com.crudproject.utils.utilInterfaces;

import com.crudproject.models.Person;

import java.util.ArrayList;

public interface IFileIOUtility {

    ArrayList<Person> readFile();

    void writeFile(ArrayList<Person> personArrayList);

}