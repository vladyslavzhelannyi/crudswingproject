package com.crudproject.utils.utilInterfaces;

import com.crudproject.models.Person;

import java.util.ArrayList;

public interface IDBUtility {

    void add(Person person);

    void delete(Person person);

    ArrayList<Person> readAll();

    void update(Person person);

}
