package com.crudproject.utils.actionListeners;

import com.crudproject.services.CRUDService;
import com.crudproject.models.jElements.jDialogs.CreatePersonDialog;
import com.crudproject.models.jElements.jDialogs.DBChoiceDialog;
import com.crudproject.models.jElements.jDialogs.ReadByIdDialog;
import com.crudproject.models.jElements.jDialogs.UpdatePersonDialog;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CrudListener implements ActionListener {
    private CRUDService crudService;
    private DBChoiceDialog dbChoiceDialog;
    private CreatePersonDialog createPersonDialog;
    private UpdatePersonDialog updatePersonDialog;
    private ReadByIdDialog readByIdDialog;
    private JTable jTable;
    private DefaultTableModel tableModel;

    public CrudListener() {

    }

    public void setJTable(JTable jTable) {
        this.jTable = jTable;
    }

    public void setTableModel(DefaultTableModel tableModel) {
        this.tableModel = tableModel;
    }

    public void setCrudService(CRUDService crudService) {
        this.crudService = crudService;
    }

    public void setDbChoiceDialog(DBChoiceDialog dbChoiceDialog) {
        this.dbChoiceDialog = dbChoiceDialog;
    }

    public void setCreatePersonDialog(CreatePersonDialog createPersonDialog) {
        this.createPersonDialog = createPersonDialog;
    }

    public void setReadByIdDialog(ReadByIdDialog readByIdDialog) {
        this.readByIdDialog = readByIdDialog;
    }

    public void setUpdatePersonDialog(UpdatePersonDialog updatePersonDialog) {
        this.updatePersonDialog = updatePersonDialog;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        switch (action) {
            case "Read data":
                dbChoiceDialog.run();
                break;
            case "Update record":
                if (jTable.getSelectedRow() != -1) {
                    updatePersonDialog.run();
                }
                break;
            case "Create record":
                if (crudService.isDbSet()) {
                    createPersonDialog.run();
                }
                break;
            case "Remove record":
                crudService.deletePerson();
                break;
            case "Read by ID":
                if (crudService.isDbSet()) {
                    readByIdDialog.run();
                }
                break;
        }
    }
}
