package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jDialogs.CreatePersonDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.utils.supportingUtils.IdGenerator;
import com.crudproject.wrappers.fillers.CreateDialogFiller;

import javax.swing.*;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.crudproject.constants.PersonConstants.*;
import static com.crudproject.constants.ValidationConstants.VALIDATED;

public class CreateListener implements ActionListener {
    private CreatePersonDialog createPersonDialog;
    private CRUDService crudService;
    private CrudValidation crudValidation;
    private IdGenerator idGenerator;
    private CreateDialogFiller createDialogFiller;
    private TablePanel tablePanel;
    private JTextField nameField;
    private JTextField surnameField;
    private JTextField ageField;
    private JTextField cityField;

    public CreateListener(CreatePersonDialog createPersonDialog, CRUDService crudService, CrudValidation crudValidation,
                          IdGenerator idGenerator, TablePanel tablePanel, CreateDialogFiller createDialogFiller) {
        this.createPersonDialog = createPersonDialog;
        this.crudService = crudService;
        this.crudValidation = crudValidation;
        this.idGenerator = idGenerator;
        this.tablePanel = tablePanel;
        this.createDialogFiller = createDialogFiller;

        this.nameField = createDialogFiller.getNameField();
        this.surnameField = createDialogFiller.getSurnameField();
        this.ageField = createDialogFiller.getAgeField();
        this.cityField = createDialogFiller.getCityField();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean isCorrect = true;
        String nameText = nameField.getText();
        String surnameText = surnameField.getText();
        String ageText = ageField.getText();
        String cityText = cityField.getText();

        nameText = nameText.trim();
        surnameText = surnameText.trim();
        ageText = ageText.trim();
        cityText = cityText.trim();

        if (checkIfInvokesAgain(nameText, surnameText, ageText, cityText)) {
            return;
        }

        String nameValidation = crudValidation.validateName(nameText);
        if (!nameValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        String surnameValidation = crudValidation.validateSurname(surnameText);
        if (!surnameValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        String ageValidation = crudValidation.validateAge(ageText);
        if (!ageValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        String cityValidation = crudValidation.validateCity(cityText);
        if (!cityValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        if (!isCorrect) {
            String nameInfo = NAME + ": " + nameValidation + "\n";
            String surnameInfo = SURNAME + ": " + surnameValidation + "\n";
            String ageInfo = AGE + ": " + ageValidation + "\n";
            String cityInfo = CITY + ": " + cityValidation + "\n";
            String errorInfo = nameInfo + surnameInfo + ageInfo + cityInfo;
            createPersonDialog.showErrorMessage(errorInfo);
            return;
        }

        int ageInt = Integer.parseInt(ageText);

        Person newPerson = new Person();

        int id = 0;
        while (id == 0) {
            int newId = idGenerator.getNewId();
            if (!tablePanel.doesIdExist(newId)) {
                id = newId;
            }
        }

        newPerson.setId(id);
        newPerson.setName(nameText);
        newPerson.setSurname(surnameText);
        newPerson.setAge(ageInt);
        newPerson.setCity(cityText);

        crudService.createPerson(newPerson);

        createPersonDialog.clearFields();
        createPersonDialog.closeDialog();
    }

    public boolean checkIfInvokesAgain(String field1, String field2, String field3, String field4) {
        if (field1.equals("") && field2.equals("") && field3.equals("") && field4.equals("")) {
            return true;
        }
        else {
            return false;
        }
    }
}
