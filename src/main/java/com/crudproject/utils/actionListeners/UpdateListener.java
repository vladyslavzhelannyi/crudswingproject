package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jDialogs.UpdatePersonDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.wrappers.fillers.UpdateDialogFiller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.crudproject.constants.PersonConstants.*;
import static com.crudproject.constants.ValidationConstants.VALIDATED;

public class UpdateListener implements ActionListener {
    private CrudValidation crudValidation;
    private CRUDService crudService;
    private UpdatePersonDialog updatePersonDialog;
    private UpdateDialogFiller updateDialogFiller;
    private JTextField nameField;
    private JTextField surnameField;
    private JTextField ageField;
    private JTextField cityField;
    private TablePanel tablePanel;

    public UpdateListener(CrudValidation crudValidation, CRUDService crudService, UpdatePersonDialog updatePersonDialog,
                          UpdateDialogFiller updateDialogFiller, TablePanel tablePanel) {
        this.crudValidation = crudValidation;
        this.crudService = crudService;
        this.updatePersonDialog = updatePersonDialog;
        this.updateDialogFiller = updateDialogFiller;
        this.tablePanel = tablePanel;

        this.nameField = updateDialogFiller.getNameField();
        this.surnameField = updateDialogFiller.getSurnameField();
        this.ageField = updateDialogFiller.getAgeField();
        this.cityField = updateDialogFiller.getCityField();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean isCorrect = true;
        String nameText = nameField.getText();
        String surnameText = surnameField.getText();
        String ageText = ageField.getText();
        String cityText = cityField.getText();
        nameText = nameText.trim();
        surnameText = surnameText.trim();
        ageText = ageText.trim();
        cityText = cityText.trim();
        String nameValidation = crudValidation.validateName(nameText);

        if (!nameValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        String surnameValidation = crudValidation.validateSurname(surnameText);

        if (!surnameValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        String ageValidation = crudValidation.validateAge(ageText);
        if (!ageValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        String cityValidation = crudValidation.validateCity(cityText);

        if (!cityValidation.equals(VALIDATED)) {
            isCorrect = false;
        }

        if (!isCorrect) {
            String nameInfo = NAME + ": " + nameValidation + "\n";
            String surnameInfo = SURNAME + ": " + surnameValidation + "\n";
            String ageInfo = AGE + ": " + ageValidation + "\n";
            String cityInfo = CITY + ": " + cityValidation + "\n";
            String errorInfo = nameInfo + surnameInfo + ageInfo + cityInfo;
            updatePersonDialog.showErrorMessage(errorInfo);
            return;
        }

        int ageInt = Integer.parseInt(ageText);
        Person personToUpdate = tablePanel.getSelectedPerson();
        personToUpdate.setId(personToUpdate.getId());
        personToUpdate.setName(nameText);
        personToUpdate.setSurname(surnameText);
        personToUpdate.setAge(ageInt);
        personToUpdate.setCity(cityText);

        crudService.updatePerson(personToUpdate);
        updatePersonDialog.closeDialog();
    }
}