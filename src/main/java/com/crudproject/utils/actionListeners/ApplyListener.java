package com.crudproject.utils.actionListeners;

import com.crudproject.caches.DBEnum;
import com.crudproject.models.jElements.jDialogs.CreatePersonDialog;
import com.crudproject.models.jElements.jDialogs.DBChoiceDialog;
import com.crudproject.services.CRUDService;
import com.crudproject.wrappers.fillers.CreateDialogFiller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.crudproject.constants.DBConstants.*;

public class ApplyListener implements ActionListener {
    private CRUDService crudService;
    private DBChoiceDialog dbChoiceDialog;
    private JComboBox<String> dbChoiceBox;

    public ApplyListener(CRUDService crudService, DBChoiceDialog dbChoiceDialog, JComboBox<String> dbChoiceBox) {
        this.crudService = crudService;
        this.dbChoiceDialog = dbChoiceDialog;
        this.dbChoiceBox = dbChoiceBox;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String dbChoice = dbChoiceBox.getSelectedItem().toString();
        switch (dbChoice) {
            case CASSANDRA_S:
                crudService.setTableData(DBEnum.CASSANDRA);
                break;
            case GRAPH_DB_S:
                crudService.setTableData(DBEnum.GRAPHDB);
                break;
            case H2_S:
                crudService.setTableData(DBEnum.H2);
                break;
            case MONGO_DB_S:
                crudService.setTableData(DBEnum.MONGODB);
                break;
            case MY_SQL_S:
                crudService.setTableData(DBEnum.MYSQL);
                break;
            case POSTGRE_SQL_S:
                crudService.setTableData(DBEnum.POSTGRESQL);
                break;
            case REDIS_S:
                crudService.setTableData(DBEnum.REDIS);
                break;
            case BINARY_S:
                crudService.setTableData(DBEnum.BINARY);
                break;
            case CSV_S:
                crudService.setTableData(DBEnum.CSV);
                break;
            case JSON_S:
                crudService.setTableData(DBEnum.JSON);
                break;
            case XML_S:
                crudService.setTableData(DBEnum.XML);
                break;
            case YAML_S:
                crudService.setTableData(DBEnum.YAML);
                break;
        }
        dbChoiceDialog.closeDialog();
    }
}