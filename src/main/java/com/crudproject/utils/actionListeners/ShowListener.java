package com.crudproject.utils.actionListeners;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jDialogs.ReadByIdDialog;
import com.crudproject.models.jElements.jFrames.CrudFrame;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.wrappers.fillers.ReadByIdDialogFiller;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowListener implements ActionListener {
    private int counter;
    private ReadByIdDialog readByIdDialog;
    private CrudValidation crudValidation;
    private JTextField idField;
    private TablePanel tablePanel;

    public ShowListener(ReadByIdDialog readByIdDialog, CrudValidation crudValidation,
                        TablePanel tablePanel, JTextField idField) {
        this.readByIdDialog = readByIdDialog;
        this.crudValidation = crudValidation;
        this.tablePanel = tablePanel;
        this.idField = idField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        counter++;
        if (counter % 2 == 0) {
            return;
        }

        String idText = idField.getText();
        boolean isId = crudValidation.isStringId(idText);
        if (!isId) {
            readByIdDialog.showMessageIncorrect();
        }
        else{
            int idInt = Integer.parseInt(idText);
            boolean doesExist = tablePanel.doesIdExist(idInt);
            if (!doesExist) {
                readByIdDialog.showMessageNotExist();
            }
            else {
                Person showPerson = tablePanel.getPersonById(idInt);
                readByIdDialog.showMessagePerson(showPerson);
            }
        }
    }
}