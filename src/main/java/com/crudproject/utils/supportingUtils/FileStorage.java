package com.crudproject.utils.supportingUtils;

import java.io.File;
import java.io.IOException;

public class FileStorage {

    public static final File DIRECTORY;

    public static final File YAML_FILE;

    public static final File JSON_FILE;

    public static final File BINARY_FILE;

    public static final File CSV_FILE;

    public static final File XML_FILE;

    static {
        DIRECTORY = new File("DataStore");

        if (!DIRECTORY.exists()) {
            DIRECTORY.mkdir();
        }

        YAML_FILE = new File("DataStore\\yaml_store.yaml");

        JSON_FILE = new File("DataStore\\json_store.json");

        BINARY_FILE = new File("DataStore\\binary_store.bin");

        CSV_FILE = new File("DataStore\\csv_store.csv");

        XML_FILE = new File("DataStore\\xml_store.xml");

        try{
            if (!YAML_FILE.exists()){
                YAML_FILE.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!JSON_FILE.exists()){
                JSON_FILE.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!BINARY_FILE.exists()){
                BINARY_FILE.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!CSV_FILE.exists()){
                CSV_FILE.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!XML_FILE.exists()){
                XML_FILE.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}