package com.crudproject.utils.supportingUtils;

import java.util.Random;

public class IdGenerator {
    private Random random;

    public IdGenerator(Random random){
        this.random = random;
    }

    public int getNewId(){
        int id = 0;
        for (int i = 0; i < 9; i++){
            int randomInt = random.nextInt(8) + 1;
            id += randomInt * Math.pow(10, i);
        }
        return id;
    }
}