package com.crudproject.utils.supportingUtils;

import static com.crudproject.constants.ValidationConstants.*;

import java.util.Locale;

public class CrudValidation {
    public static final String INCORRECT_INPUT = "Неверный ввод";
    public static final String READ = "read";
    public static final String UPDATE = "update";
    public static final String CREATE = "create";
    public static final String DELETE = "delete";
    public static final String HELP = "help";
    public static final String EXIT = "exit";
    public static final String START = "start";
    public static final String SWITCH = "switch";
    public static final String ALL = "all";

    public String transformEnvironment(String environment) {
        environment = environment.trim();
        environment = environment.toLowerCase(Locale.ROOT);
        return environment;
    }

    public String validateName(String name){
        String validation = null;
        String namePatternEng = "[A-Z]{1}[a-z]*[-]?[A-Z]?[a-z]*";
        String namePatternRu = "[А-Я]{1}[а-я]*[-]?[А-Я]?[а-я]*";
        String symbols = "[А-Яа-яA-Za-z]+[-]*";
        if (name.length() > 1500){
            validation = "Слишком длинный ввод. Ограничение: 1500 символовю";
        }
        else {
            if (name.matches(namePatternEng) || name.matches(namePatternRu)){
                validation = VALIDATED;
            }
            else if (name.matches(symbols)){
                validation = "Неверный ввод.";
            }
            else {
                validation = "Вы ввели некорректные символы.";
            }
        }
        return validation;
    }

    public String validateSurname(String name){
        String validation = null;
        String namePatternEng = "[A-Z]{1}[a-z]*[-]?[A-Z]?[a-z]*";
        String namePatternRu = "[А-Я]{1}[а-я]*[-]?[А-Я]?[а-я]*";
        String symbols = "[А-Яа-яA-Za-z]+[-]*";
        if (name.length() > 1500){
            validation = "Слишком длинный ввод. Ограничение: 1500 символовю";
        }
        else {
            if (name.matches(namePatternEng) || name.matches(namePatternRu)){
                validation = VALIDATED;
            }
            else if (name.matches(symbols)){
                validation = "Неверный ввод.";
            }
            else {
                validation = "Вы ввели некорректные символы.";
            }
        }
        return validation;
    }

    public String validateAge(String age){
        String validation = null;
        String agePattern = "[0-9]+";
        if (age.matches(agePattern)){
            int ageInt = Integer.parseInt(age);
            if (ageInt > 200) {
                validation = "Слишком большое число";
            }
            else if (ageInt < 0) {
                validation = "Возраст не может быть отрицательным";
            }
            else {
                validation = VALIDATED;
            }
        }
        else {
            validation = "Вы ввели некорректные символы.";
        }
        return validation;
    }

    public String validateCity(String city){
        String validation = null;
        String cityPatternEng = "[A-Z]{1}[a-z]*[-]?[ ]?[A-Z]?[a-z]*";
        String cityPatternRu = "[А-Я]{1}[а-я]*[-]?[ ]?[А-Я]?[а-я]*";
        String symbols = "[А-Яа-яA-Za-z]+[-]*";
        if (city.length() > 168){
            validation = "Слишком длинный ввод. Ограничение: 168 символов.";
        }
        else {
            if (city.matches(cityPatternEng) || city.matches(cityPatternRu)){
                validation = VALIDATED;
            }
            else if (city.matches(symbols)){
                validation = "Неверный ввод.";
            }
            else {
                validation = "Вы ввели некорректные символы.";
            }
        }
        return validation;
    }

    public boolean isStringId(String string){
        String idPattern = "[0-9]{9}";
        boolean isId = string.matches(idPattern);
        return isId;
    }
}
