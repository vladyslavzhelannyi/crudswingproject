package com.crudproject.utils.supportingUtils;

import com.crudproject.constants.AgeFilterConstants;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

import static com.crudproject.constants.AgeFilterConstants.*;

public class Filters {
    public static final RowFilter<Object, Object> NO_FILTER = new RowFilter<Object, Object>() {
        public boolean include(Entry entry) {
            return true;
        }
    };

    public static final RowFilter<Object, Object> FILTER_018 = new RowFilter<Object, Object>() {
        public boolean include(Entry entry) {
            int age = Integer.parseInt((String) entry.getValue(3));
            return age <= 18;
        }
    };

    public static final RowFilter<Object, Object> FILTER_1935 = new RowFilter<Object, Object>() {
        public boolean include(Entry entry) {
            int age = Integer.parseInt((String) entry.getValue(3));
            return age >= 19 && age <= 35;
        }
    };

    public static final RowFilter<Object, Object> FILTER_3655 = new RowFilter<Object, Object>() {
        public boolean include(Entry entry) {
            int age = Integer.parseInt((String) entry.getValue(3));
            return age >= 36 && age <= 55;
        }
    };

    public static final RowFilter<Object, Object> FILTER_56 = new RowFilter<Object, Object>() {
        public boolean include(Entry entry) {
            int age = Integer.parseInt((String) entry.getValue(3));
            return age >= 56;
        }
    };

    public static final Map<String, RowFilter<Object, Object>> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put(AgeFilterConstants.NO_FILTER, NO_FILTER);
        FILTER_MAP.put(FILTER018, FILTER_018);
        FILTER_MAP.put(FILTER1935, FILTER_1935);
        FILTER_MAP.put(FILTER3655, FILTER_3655);
        FILTER_MAP.put(FILTER56, FILTER_56);
    }

}
