package com.crudproject.utils.supportingUtils;

import com.crudproject.caches.DBEnum;
import com.crudproject.utils.fileIOUtilities.*;
import com.crudproject.utils.iDBUtilityImplementations.dbUtilities.*;
import com.crudproject.utils.iDBUtilityImplementations.fileUtilities.*;
import com.crudproject.utils.utilInterfaces.IDBUtility;
import com.crudproject.wrappers.ObjectInputStreamWrapper;
import com.crudproject.wrappers.ObjectOutputStreamWrapper;
import com.crudproject.wrappers.WrapperMongo;
import com.crudproject.wrappers.WrapperRedis;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class DBUtilitiesMap {
    public static final Map<DBEnum, IDBUtility> DB_MAP = new HashMap<>();

    //ПЕРЕПИСАТЬ С ПОМОЩЬЮ SINGLETON

    private static final ObjectOutputStreamWrapper OBJECT_OUTPUT_STREAM_WRAPPER = new ObjectOutputStreamWrapper();

    private static final ObjectInputStreamWrapper OBJECT_INPUT_STREAM_WRAPPER = new ObjectInputStreamWrapper();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final ObjectMapper YAML_MAPPER = new ObjectMapper(new YAMLFactory());

    private static final XmlMapper XML_MAPPER = new XmlMapper();

    private static final WrapperMongo WRAPPER_MONGO = new WrapperMongo();

    private static final WrapperRedis WRAPPER_REDIS = new WrapperRedis();

    private static final Gson GSON = new Gson();

    private static final CassandraUtility CASSANDRA_UTILITY = new CassandraUtility();

    private static final GraphDBUtility GRAPH_DB_UTILITY = new GraphDBUtility();

    private static final H2Utility H2_UTILITY = new H2Utility();

    private static final MongoDBUtility MONGO_DB_UTILITY = new MongoDBUtility(WRAPPER_MONGO);

    private static final MySqlUtility MY_SQL_UTILITY = new MySqlUtility();

    private static final PostgreSqlUtility POSTGRE_SQL_UTILITY = new PostgreSqlUtility();

    private static final RedisUtility REDIS_UTILITY = new RedisUtility(WRAPPER_REDIS, GSON);

    private static final BinaryFileUtility BINARY_UTILITY =
            new BinaryFileUtility(new BinaryIOUtility(FileStorage.BINARY_FILE, OBJECT_INPUT_STREAM_WRAPPER,
                    OBJECT_OUTPUT_STREAM_WRAPPER));

    private static final CsvFileUtility CSV_FILE_UTILITY =
            new CsvFileUtility(new CsvIOUtility(FileStorage.CSV_FILE));

    private static final JsonFileUtility JSON_FILE_UTILITY =
            new JsonFileUtility(new JsonIOUtility(FileStorage.JSON_FILE, OBJECT_MAPPER));

    private static final XmlFileUtility XML_FILE_UTILITY =
            new XmlFileUtility(new XmlIOUtility(FileStorage.XML_FILE, XML_MAPPER));

    private static final YamlFileUtility YAML_FILE_UTILITY =
            new YamlFileUtility(new YamlIOUtility(FileStorage.YAML_FILE, YAML_MAPPER));

    static {
        DB_MAP.put(DBEnum.CASSANDRA, CASSANDRA_UTILITY);
        DB_MAP.put(DBEnum.GRAPHDB, GRAPH_DB_UTILITY);
        DB_MAP.put(DBEnum.H2, H2_UTILITY);
        DB_MAP.put(DBEnum.MONGODB, MONGO_DB_UTILITY);
        DB_MAP.put(DBEnum.MYSQL, MY_SQL_UTILITY);
        DB_MAP.put(DBEnum.POSTGRESQL, POSTGRE_SQL_UTILITY);
        DB_MAP.put(DBEnum.REDIS, REDIS_UTILITY);
        DB_MAP.put(DBEnum.BINARY, BINARY_UTILITY);
        DB_MAP.put(DBEnum.CSV, CSV_FILE_UTILITY);
        DB_MAP.put(DBEnum.JSON, JSON_FILE_UTILITY);
        DB_MAP.put(DBEnum.XML, XML_FILE_UTILITY);
        DB_MAP.put(DBEnum.YAML, YAML_FILE_UTILITY);
    }
}
