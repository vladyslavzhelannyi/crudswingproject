package com.crudproject.constants;

public class PersonConstants {

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String SURNAME = "surname";

    public static final String AGE = "age";

    public static final String CITY = "city";

}
