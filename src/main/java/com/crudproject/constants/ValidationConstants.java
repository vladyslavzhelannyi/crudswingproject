package com.crudproject.constants;

public class ValidationConstants {

    public static final String VALIDATED = "validated";

    public static final String INCORRECT_INPUT = "Неверный ввод.";

    public static final String INCORRECT_SYMBOLS = "Вы ввели некорректные символы.";


}
