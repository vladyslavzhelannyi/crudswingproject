package com.crudproject.constants;

public class AgeFilterConstants {

    public static final String NO_FILTER = "no filter";

    public static final String FILTER018 = "0-18";

    public static final String FILTER1935 = "19-35";

    public static final String FILTER3655 = "36-55";

    public static final String FILTER56 = ">55";

}
