package com.crudproject.constants;

public class DBConstants {

    public static final String JSON_S = "json";

    public static final String YAML_S = "yaml";

    public static final String BINARY_S = "binary";

    public static final String XML_S = "xml";

    public static final String CSV_S = "csv";

    public static final String MY_SQL_S = "mysql";

    public static final String POSTGRE_SQL_S = "postgresql";

    public static final String H2_S = "h2";

    public static final String MONGO_DB_S = "mongodb";

    public static final String REDIS_S = "redis";

    public static final String CASSANDRA_S = "cassandra";

    public static final String GRAPH_DB_S = "graphdb";

}