package com.crudproject;

import static com.crudproject.constants.AgeFilterConstants.*;
import static com.crudproject.utils.supportingUtils.Filters.FILTER_MAP;

import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.CrudListener;
import com.crudproject.utils.factories.DBUtilsFactory;
import com.crudproject.utils.itemListeners.FilterListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.utils.supportingUtils.DBUtilitiesMap;
import com.crudproject.utils.supportingUtils.IdGenerator;
import com.crudproject.wrappers.fillers.*;
import com.crudproject.wrappers.listenWrappers.ApplyListenerWrapper;
import com.crudproject.wrappers.listenWrappers.CreateListenerWrapper;
import com.crudproject.wrappers.listenWrappers.ShowListenerWrapper;
import com.crudproject.wrappers.listenWrappers.UpdateListenerWrapper;

import javax.swing.*;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        String[] filterBoxText = new String[] {NO_FILTER, FILTER018, FILTER1935, FILTER3655, FILTER56};
        JComboBox<String> filterBox = new JComboBox<>(filterBoxText);


        Object[] columnNames = new String[] {"id", "name", "surname", "age", "city"};
        DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JTable jTable = new JTable(tableModel);
        CrudValidation crudValidation = new CrudValidation();
        Random random = new Random();
        IdGenerator idGenerator = new IdGenerator(random);

        DBUtilsFactory dbUtilsFactory = new DBUtilsFactory(DBUtilitiesMap.DB_MAP);
        CrudListener crudListener = new CrudListener();
        TablePanel tablePanel = new TablePanel(jTable, tableModel);

        CreateDialogFiller createDialogFiller = new CreateDialogFiller();
        CrudFrameFiller crudFrameFiller = new CrudFrameFiller();
        DBChoiceDialogFiller dbChoiceDialogFiller = new DBChoiceDialogFiller();
        ReadByIdDialogFiller readByIdDialogFiller = new ReadByIdDialogFiller();
        UpdateDialogFiller updateDialogFiller = new UpdateDialogFiller();

        FilterListener filterListener = new FilterListener(FILTER_MAP, tablePanel);

        ApplyListenerWrapper applyListenerWrapper = new ApplyListenerWrapper();
        CreateListenerWrapper createListenerWrapper = new CreateListenerWrapper();
        ShowListenerWrapper showListenerWrapper = new ShowListenerWrapper();
        UpdateListenerWrapper updateListenerWrapper = new UpdateListenerWrapper();

        CRUDService crudService = new CRUDService(dbUtilsFactory, crudListener, jTable, tableModel, crudValidation,
                idGenerator, filterBox, FILTER_MAP, tablePanel, createDialogFiller, dbChoiceDialogFiller,
                readByIdDialogFiller, updateDialogFiller, crudFrameFiller, filterListener, applyListenerWrapper,
                createListenerWrapper, showListenerWrapper, updateListenerWrapper);
        crudService.run();
    }
}
