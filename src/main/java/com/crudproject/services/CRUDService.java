package com.crudproject.services;

import com.crudproject.caches.DBEnum;
import static com.crudproject.constants.AgeFilterConstants.*;
import com.crudproject.models.Person;
import com.crudproject.models.jElements.jFrames.CrudFrame;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.utils.actionListeners.ApplyListener;
import com.crudproject.utils.actionListeners.CreateListener;
import com.crudproject.utils.actionListeners.CrudListener;
import com.crudproject.utils.actionListeners.ShowListener;
import com.crudproject.utils.factories.DBUtilsFactory;
import com.crudproject.models.jElements.jDialogs.CreatePersonDialog;
import com.crudproject.models.jElements.jDialogs.DBChoiceDialog;
import com.crudproject.models.jElements.jDialogs.ReadByIdDialog;
import com.crudproject.models.jElements.jDialogs.UpdatePersonDialog;
import com.crudproject.utils.itemListeners.FilterListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.utils.supportingUtils.IdGenerator;
import com.crudproject.utils.utilInterfaces.IDBUtility;
import com.crudproject.wrappers.fillers.*;
import com.crudproject.wrappers.listenWrappers.ApplyListenerWrapper;
import com.crudproject.wrappers.listenWrappers.CreateListenerWrapper;
import com.crudproject.wrappers.listenWrappers.ShowListenerWrapper;
import com.crudproject.wrappers.listenWrappers.UpdateListenerWrapper;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Map;

public class CRUDService{
    private DBUtilsFactory dbUtilsFactory;
    private IDBUtility dbUtility;
    private TablePanel tablePanel;
    private CrudFrame crudFrame;

    private JTable jTable;
    private DefaultTableModel model;
    private CrudListener crudListener;
    private DBChoiceDialog dbChoiceDialog;
    private CreatePersonDialog createPersonDialog;
    private UpdatePersonDialog updatePersonDialog;
    private ReadByIdDialog readByIdDialog;
    private CreateDialogFiller createDialogFiller;
    private DBChoiceDialogFiller dbChoiceDialogFiller;
    private ReadByIdDialogFiller readByIdDialogFiller;
    private UpdateDialogFiller updateDialogFiller;
    private CrudFrameFiller crudFrameFiller;
    private ApplyListenerWrapper applyListenerWrapper;
    private CreateListenerWrapper createListenerWrapper;
    private ShowListenerWrapper showListenerWrapper;
    private UpdateListenerWrapper updateListenerWrapper;
    private FilterListener filterListener;
    private CrudValidation crudValidation;
    private IdGenerator idGenerator;
    private JComboBox<String> filterBox;
    private Map<String, RowFilter<Object, Object>> filterMap;

    private int windowWidth = 1200;
    private int windowHeight = 600;

    public CRUDService(DBUtilsFactory dbUtilsFactory, CrudListener crudListener, JTable jTable, DefaultTableModel model,
                       CrudValidation crudValidation, IdGenerator idGenerator, JComboBox<String> filterBox,
                       Map<String, RowFilter<Object, Object>> filterMap, TablePanel tablePanel,
                       CreateDialogFiller createDialogFiller, DBChoiceDialogFiller dbChoiceDialogFiller,
                       ReadByIdDialogFiller readByIdDialogFiller, UpdateDialogFiller updateDialogFiller,
                       CrudFrameFiller crudFrameFiller, FilterListener filterListener,
                       ApplyListenerWrapper applyListenerWrapper, CreateListenerWrapper createListenerWrapper,
                       ShowListenerWrapper showListenerWrapper, UpdateListenerWrapper updateListenerWrapper) {
        this.jTable = jTable;
        this.model = model;
        this.dbUtilsFactory = dbUtilsFactory;
        this.crudListener = crudListener;
        this.idGenerator = idGenerator;
        this.crudValidation = crudValidation;
        this.filterBox = filterBox;
        this.filterMap = filterMap;
        this.tablePanel = tablePanel;
        this.filterListener = filterListener;

        this.createDialogFiller = createDialogFiller;
        this.dbChoiceDialogFiller = dbChoiceDialogFiller;
        this.readByIdDialogFiller = readByIdDialogFiller;
        this.updateDialogFiller = updateDialogFiller;
        this.crudFrameFiller = crudFrameFiller;

        this.applyListenerWrapper = applyListenerWrapper;
        this.createListenerWrapper = createListenerWrapper;
        this.showListenerWrapper = showListenerWrapper;
        this.updateListenerWrapper = updateListenerWrapper;

        this.crudFrame = new CrudFrame(crudListener, filterListener, crudFrameFiller, tablePanel);

        dbChoiceDialog = new DBChoiceDialog(this, crudFrame, dbChoiceDialogFiller, applyListenerWrapper);
        createPersonDialog = new CreatePersonDialog(this, crudFrame, crudValidation, idGenerator, tablePanel,
                createDialogFiller, createListenerWrapper);
        updatePersonDialog = new UpdatePersonDialog(this, crudFrame, crudValidation, updateDialogFiller, tablePanel,
                updateListenerWrapper);
        readByIdDialog = new ReadByIdDialog(crudFrame, crudValidation, tablePanel, readByIdDialogFiller,
                showListenerWrapper);

        //filling crud_listener
        this.crudListener.setCrudService(this);
        this.crudListener.setDbChoiceDialog(dbChoiceDialog);
        this.crudListener.setJTable(jTable);
        this.crudListener.setTableModel(model);
        this.crudListener.setCreatePersonDialog(createPersonDialog);
        this.crudListener.setUpdatePersonDialog(updatePersonDialog);
        this.crudListener.setReadByIdDialog(readByIdDialog);
    }

    public void run() {
        crudFrame.run();
    }

    public boolean isDbSet() {
        if (dbUtility == null) {
            return false;
        }
        return true;
    }

    public void setTableData(DBEnum dbEnum) {
        dbUtility = dbUtilsFactory.getDBUtil(dbEnum);
        ArrayList<Person> dbListPeople = dbUtility.readAll();
        tablePanel.setDataStorage(dbListPeople);
    }

    public void createPerson(Person person) {
        dbUtility.add(person);
        tablePanel.addPersonTable(person);
    }

    public void updatePerson(Person person) {
        dbUtility.update(person);
        tablePanel.updatePersonTable(person);
    }

    public void deletePerson() {
        int idx = jTable.getSelectedRow();
        if (idx != -1) {
            Person personToDelete = tablePanel.getSelectedPerson();
            dbUtility.delete(personToDelete);
            tablePanel.deletePersonFromTable(idx);
        }
    }
}
