package com.crudproject.configs;

import com.crudproject.exceptions.FailedConnectionException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnectionConfig {

    private static final String URL_NEO4J = "jdbc:neo4j:bolt://localhost:7687";
    private static final String USER_NEO4J = "Alyona1";
    private static final String PASSWORD_NEO4J = "123";

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/crud";
    private static final String USER = "postgres";
    private static final String PASSWORD = "tasadar123";

    private static final String URL_SQL = "jdbc:mysql://localhost:3306/MySql";
    private static final String USER_SQL = "Alyona";
    private static final String PASSWORD_SQL = "root";

    private static final String URL_H2 = "jdbc:h2:D:\\java eclipse\\H2;MV_STORE=false";
    private static final String USER_H2 = "Alyona";
    private static final String PASSWORD_H2 = "123";

    public static Connection getConnectionToNeo4j() throws FailedConnectionException, SQLException {
        Connection connection = DriverManager.getConnection(URL_NEO4J, USER_NEO4J, PASSWORD_NEO4J);
        return connection;
    }

    public static Connection getConnectionToH2() throws FailedConnectionException {
        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection(URL_H2, USER_H2, PASSWORD_H2);
        } catch (SQLException | ClassNotFoundException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }

    public static Connection getConnectionToMySql() throws FailedConnectionException {
        try {
            return DriverManager.getConnection(URL_SQL, USER_SQL, PASSWORD_SQL);
        } catch (SQLException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }

    public static Connection getConnectionToPostgres() throws FailedConnectionException {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }
}