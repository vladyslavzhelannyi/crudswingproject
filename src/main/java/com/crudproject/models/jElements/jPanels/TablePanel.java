package com.crudproject.models.jElements.jPanels;

import com.crudproject.caches.DBEnum;
import com.crudproject.models.Person;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.util.ArrayList;

import static com.crudproject.constants.AgeFilterConstants.NO_FILTER;

public class TablePanel extends JScrollPane {
    private JTable jTable;
    private DefaultTableModel model;

    public TablePanel(JTable jTable, DefaultTableModel model) {
        super(jTable);
        this.jTable = jTable;
        this.model = model;
    }

    public String[] convertPersonStr(Person person) {
        String[] personStr = new String[5];
        personStr[0] = String.valueOf(person.getId());
        personStr[1] = person.getName();
        personStr[2] = person.getSurname();
        personStr[3] = String.valueOf(person.getAge());
        personStr[4] = person.getCity();
        return personStr;
    }

    public void setDataStorage(ArrayList<Person> dbListPeople) {
        model.setRowCount(0);
        addPeople(dbListPeople);
    }

    public void addPeople(ArrayList<Person> people) {
        if (people != null) {
            for (Person person : people) {
                addPersonTable(person);
            }
        }
    }

    public void addPersonTable(Person person) {
        String[] personToAdd = convertPersonStr(person);
        model.insertRow(0, personToAdd);
    }

    public void updatePersonTable(Person person) {
        String idStr = String.valueOf(person.getId());
        String name = person.getName();
        String surname = person.getSurname();
        String ageStr = String.valueOf(person.getAge());
        String city = String.valueOf(person.getCity());
        int rowNum = jTable.getRowCount();
        for (int i = 0; i < rowNum; i++) {
            String ids = (String) jTable.getValueAt(i, 0);
            if (idStr.equals(ids)) {
                model.setValueAt(name, i, 1);
                model.setValueAt(surname, i, 2);
                model.setValueAt(ageStr, i, 3);
                model.setValueAt(city, i, 4);
                break;
            }
        }
    }

    public Person getSelectedPerson() {
        int idx = jTable.getSelectedRow();
        if (idx == -1) {
            return null;
        }

        Person selectedPerson = new Person();

        String idStr = (String) jTable.getValueAt(idx, 0);
        String nameStr = (String) jTable.getValueAt(idx, 1);
        String surnameStr = (String) jTable.getValueAt(idx, 2);
        String ageStr = (String) jTable.getValueAt(idx, 3);
        String cityStr = (String) jTable.getValueAt(idx, 4);

        int idInt = Integer.parseInt(idStr);
        int ageInt = Integer.parseInt(ageStr);

        selectedPerson.setId(idInt);
        selectedPerson.setName(nameStr);
        selectedPerson.setSurname(surnameStr);
        selectedPerson.setAge(ageInt);
        selectedPerson.setCity(cityStr);

        return selectedPerson;
    }

    public boolean isSomeRowSelected() {
        int idx = jTable.getSelectedRow();
        if (idx == -1) {
            return false;
        }
        else {
            return true;
        }
    }

    public void deletePersonFromTable(int idx) {
        if (idx != -1) {
            model.removeRow(idx);
        }
    }

    public boolean doesIdExist(int id) {
        int rowNum = jTable.getRowCount();
        for (int i = 0; i < rowNum; i++) {
            String idStr = (String) jTable.getValueAt(i, 0);
            int idInt = Integer.parseInt(idStr);
            if (idInt == id) {
                return true;
            }
        }
        return false;
    }

    public Person getPersonById(int id) {
        Person person = null;
        int rowNum = jTable.getRowCount();
        for (int i = 0; i < rowNum; i++) {
            String idStr = (String) jTable.getValueAt(i, 0);
            int idInt = Integer.parseInt(idStr);
            if (idInt == id) {
                person = new Person();
                person.setId(id);
                person.setName((String) jTable.getValueAt(i, 1));
                person.setSurname((String) jTable.getValueAt(i, 2));
                person.setAge(Integer.parseInt((String) jTable.getValueAt(i, 3)));
                person.setCity((String) jTable.getValueAt(i, 4));
                break;
            }
        }
        return person;
    }

    public void filerTable(RowFilter<Object, Object> filter) {
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
        sorter.setRowFilter(filter);
        jTable.setRowSorter(sorter);
    }
}
