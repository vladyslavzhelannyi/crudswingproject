package com.crudproject.models.jElements.jDialogs;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jFrames.CrudFrame;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.ShowListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.wrappers.fillers.ReadByIdDialogFiller;
import com.crudproject.wrappers.listenWrappers.ShowListenerWrapper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.crudproject.constants.PersonConstants.*;
import static com.crudproject.constants.PersonConstants.CITY;

public class ReadByIdDialog extends JDialog {
    private ReadByIdDialogFiller readByIdDialogFiller;
    private CrudValidation crudValidation;
    private TablePanel tablePanel;
    private ShowListener showListener;
    private ShowListenerWrapper showListenerWrapper;
    private int windowWidth = 200;
    private int windowHeight = 100;
    private JTextField idField;
    private JButton showButton;

    public ReadByIdDialog(CrudFrame crudFrame, CrudValidation crudValidation, TablePanel tablePanel,
                          ReadByIdDialogFiller readByIdDialogFiller, ShowListenerWrapper showListenerWrapper) {
        super(crudFrame, "Read by id", Dialog.ModalityType.DOCUMENT_MODAL);
        this.readByIdDialogFiller = readByIdDialogFiller;
        this.crudValidation = crudValidation;
        this.tablePanel = tablePanel;
        this.showListenerWrapper = showListenerWrapper;
    }

    public void run() {
        this.idField = readByIdDialogFiller.getIdField();
        this.showButton = readByIdDialogFiller.getShowButton();
        this.showListener = showListenerWrapper.getShowListener(this, crudValidation, tablePanel, idField);
        setPosition();
        this.setLayout(new GridLayout(2, 1));
        this.showButton.addActionListener(showListener);
        this.add(idField);
        this.add(showButton);
        this.setVisible(true);
    }

    public void showMessageIncorrect() {
        JOptionPane.showMessageDialog(this, "Некорректные символы");
    }

    public void showMessageNotExist() {
        JOptionPane.showMessageDialog(this, "Данного id нет");
    }

    public void showMessagePerson(Person person) {
        String nameInfo = NAME + ": " + person.getName() + "\n";
        String surnameInfo = SURNAME + ": " + person.getSurname() + "\n";
        String ageInfo = AGE + ": " + person.getAge() + "\n";
        String cityInfo = CITY + ": " + person.getCity() + "\n";

        String personInfo = nameInfo + surnameInfo + ageInfo + cityInfo;

        JOptionPane.showMessageDialog(this, personInfo);
    }

    private void setPosition() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth,
                windowHeight);
    }
}
