package com.crudproject.models.jElements.jDialogs;

import com.crudproject.caches.DBEnum;
import com.crudproject.models.jElements.jFrames.CrudFrame;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.ApplyListener;
import com.crudproject.wrappers.fillers.DBChoiceDialogFiller;
import com.crudproject.wrappers.listenWrappers.ApplyListenerWrapper;

import static com.crudproject.constants.DBConstants.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DBChoiceDialog extends JDialog {
    private ApplyListener applyListener;
    private DBChoiceDialogFiller dbChoiceDialogFiller;
    private CRUDService crudService;
    private ApplyListenerWrapper applyListenerWrapper;
    private int windowWidth = 200;
    private int windowHeight = 100;

    private JComboBox<String> dbChoiceBox;
    private JButton dbApply;

    public DBChoiceDialog(CRUDService crudService, CrudFrame crudFrame, DBChoiceDialogFiller dbChoiceDialogFiller,
                          ApplyListenerWrapper applyListenerWrapper) {
        super(crudFrame, "Data storage choice", Dialog.ModalityType.DOCUMENT_MODAL);
        this.dbChoiceDialogFiller = dbChoiceDialogFiller;
        this.crudService = crudService;
        this.applyListenerWrapper = applyListenerWrapper;
    }

    public void run() {
        dbChoiceBox = dbChoiceDialogFiller.getDbChoiceBox();
        applyListener = applyListenerWrapper.getApplyListener(crudService, this, dbChoiceBox);

        setPosition();

        dbApply = dbChoiceDialogFiller.getDbApplyButton();

        dbApply.addActionListener(applyListener);
        Container container = this.getContentPane();
        container.setLayout(new GridLayout(2, 1));
        container.add(dbChoiceBox);
        container.add(dbApply);
        this.setVisible(true);
    }


    public void closeDialog() {
        this.setVisible(false);
    }

    private void setPosition() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth,
                windowHeight);
    }
}
