package com.crudproject.models.jElements.jDialogs;

import static com.crudproject.constants.ValidationConstants.VALIDATED;

import static com.crudproject.constants.PersonConstants.*;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jFrames.CrudFrame;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.CreateListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.utils.supportingUtils.IdGenerator;
import com.crudproject.wrappers.fillers.CreateDialogFiller;
import com.crudproject.wrappers.listenWrappers.CreateListenerWrapper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreatePersonDialog extends JDialog {
    private CreateDialogFiller createDialogFiller;
    private CreateListener createListener;
    private CRUDService crudService;
    private TablePanel tablePanel;
    private CrudValidation crudValidation;
    private IdGenerator idGenerator;
    private CreateListenerWrapper createListenerWrapper;
    private JPanel namePanel = new JPanel();
    private JPanel surnamePanel = new JPanel();
    private JPanel agePanel = new JPanel();
    private JPanel cityPanel = new JPanel();
    private JLabel nameLabel = new JLabel("name :");
    private JLabel surnameLabel = new JLabel("surname :");
    private JLabel ageLabel = new JLabel("age :");
    private JLabel cityLabel = new JLabel("city :");
    private JTextField nameField;
    private JTextField surnameField;
    private JTextField ageField;
    private JTextField cityField;
    private JButton createButton;

    private int windowWidth = 300;
    private int windowHeight = 250;

    public CreatePersonDialog(CRUDService crudService, CrudFrame crudFrame, CrudValidation crudValidation,
                              IdGenerator idGenerator, TablePanel tablePanel, CreateDialogFiller createDialogFiller,
                              CreateListenerWrapper createListenerWrapper) {
        super(crudFrame, "Registration window", Dialog.ModalityType.DOCUMENT_MODAL);
        this.createDialogFiller = createDialogFiller;
        this.crudService = crudService;
        this.tablePanel = tablePanel;
        this.crudValidation = crudValidation;
        this.idGenerator = idGenerator;
        this.createListenerWrapper = createListenerWrapper;
    }

    public void run() {
        this.createListener =
                createListenerWrapper.getCreateListener(this, crudService, crudValidation, idGenerator, tablePanel,
                createDialogFiller);
        this.setLayout(new GridLayout(5, 1));

        this.nameField = createDialogFiller.getNameField();
        this.surnameField = createDialogFiller.getSurnameField();
        this.ageField = createDialogFiller.getAgeField();
        this.cityField = createDialogFiller.getCityField();
        this.createButton = createDialogFiller.getCreateButton();

        GridLayout fieldLayout = new GridLayout(1, 2);

        createButton.addActionListener(createListener);

        namePanel.setLayout(fieldLayout);
        surnamePanel.setLayout(fieldLayout);
        agePanel.setLayout(fieldLayout);
        cityPanel.setLayout(fieldLayout);

        namePanel.add(nameLabel);
        namePanel.add(nameField);

        surnamePanel.add(surnameLabel);
        surnamePanel.add(surnameField);

        agePanel.add(ageLabel);
        agePanel.add(ageField);

        cityPanel.add(cityLabel);
        cityPanel.add(cityField);

        this.add(namePanel);
        this.add(surnamePanel);
        this.add(agePanel);
        this.add(cityPanel);

        this.add(createButton);
        setPosition();

        this.setVisible(true);
    }

    public void clearFields() {
        nameField.setText("");
        surnameField.setText("");
        ageField.setText("");
        cityField.setText("");
    }

    public void closeDialog() {
        this.setVisible(false);
    }

    public void showErrorMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    private void setPosition() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth,
                windowHeight);
    }
}
