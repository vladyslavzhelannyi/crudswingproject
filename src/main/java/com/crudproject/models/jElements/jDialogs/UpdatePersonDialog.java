package com.crudproject.models.jElements.jDialogs;

import com.crudproject.models.Person;
import com.crudproject.models.jElements.jFrames.CrudFrame;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.UpdateListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.wrappers.fillers.UpdateDialogFiller;
import com.crudproject.wrappers.listenWrappers.UpdateListenerWrapper;

import javax.swing.*;
import java.awt.*;

public class UpdatePersonDialog extends JDialog {
    private TablePanel tablePanel;
    private UpdateListener updateListener;
    private UpdateDialogFiller updateDialogFiller;
    private CRUDService crudService;
    private CrudValidation crudValidation;
    private UpdateListenerWrapper updateListenerWrapper;
    JPanel namePanel = new JPanel();
    JPanel surnamePanel = new JPanel();
    JPanel agePanel = new JPanel();
    JPanel cityPanel = new JPanel();

    JLabel nameLabel = new JLabel("name :");
    JLabel surnameLabel = new JLabel("surname :");
    JLabel ageLabel = new JLabel("age :");
    JLabel cityLabel = new JLabel("city :");

    JTextField nameField;
    JTextField surnameField;
    JTextField ageField;
    JTextField cityField;

    JButton updateButton;

    private int windowWidth = 300;
    private int windowHeight = 250;

    public UpdatePersonDialog(CRUDService crudService, CrudFrame crudFrame, CrudValidation crudValidation,
                              UpdateDialogFiller updateDialogFiller, TablePanel tablePanel,
                              UpdateListenerWrapper updateListenerWrapper) {
        super(crudFrame, "UpdateWindow", Dialog.ModalityType.DOCUMENT_MODAL);
        this.tablePanel = tablePanel;
        this.updateDialogFiller = updateDialogFiller;
        this.crudService = crudService;
        this.crudValidation = crudValidation;
        this.updateListenerWrapper = updateListenerWrapper;
    }

    public void run() {
        this.updateListener =
                updateListenerWrapper.getUpdateListener(crudValidation, crudService, this, updateDialogFiller,
                        tablePanel);

        this.setLayout(new GridLayout(5, 1));

        GridLayout fieldLayout = new GridLayout(1, 2);

        this.nameField = updateDialogFiller.getNameField();
        this.surnameField = updateDialogFiller.getSurnameField();
        this.ageField = updateDialogFiller.getAgeField();
        this.cityField = updateDialogFiller.getCityField();
        this.updateButton = updateDialogFiller.getUpdateButton();

        updateButton.addActionListener(updateListener);

        namePanel.setLayout(fieldLayout);
        surnamePanel.setLayout(fieldLayout);
        agePanel.setLayout(fieldLayout);
        cityPanel.setLayout(fieldLayout);

        namePanel.add(nameLabel);
        namePanel.add(nameField);

        surnamePanel.add(surnameLabel);
        surnamePanel.add(surnameField);

        agePanel.add(ageLabel);
        agePanel.add(ageField);

        cityPanel.add(cityLabel);
        cityPanel.add(cityField);

        this.add(namePanel);
        this.add(surnamePanel);
        this.add(agePanel);
        this.add(cityPanel);

        this.add(updateButton);

        setPosition();


        Person personToUpdate = tablePanel.getSelectedPerson();
        String name = personToUpdate.getName();
        String surname = personToUpdate.getSurname();
        String age = String.valueOf(personToUpdate.getAge());
        String city = String.valueOf(personToUpdate.getCity());

        nameField.setText(name);
        surnameField.setText(surname);
        ageField.setText(age);
        cityField.setText(city);

        this.setVisible(true);
    }

    public void closeDialog() {
        this.setVisible(false);
    }

    public void showErrorMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    private void setPosition() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth,
                windowHeight);
    }
}
