package com.crudproject.models.jElements.jFrames;

import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.CrudListener;
import com.crudproject.utils.itemListeners.FilterListener;
import com.crudproject.wrappers.fillers.CrudFrameFiller;

import javax.swing.*;
import java.awt.*;

public class CrudFrame extends JFrame {
    private JPanel buttonPanel = new JPanel();
    private JButton readButton;
    private JButton updateButton;
    private JButton createButton;
    private JButton removeButton;
    private JButton readIdButton;
    private TablePanel tablePanel;
    private CrudListener crudListener;
    private JComboBox<String> filterBox;
    private FilterListener filterListener;
    private CrudFrameFiller crudFrameFiller;

    private int windowWidth = 1200;
    private int windowHeight = 635;

    public CrudFrame(CrudListener crudListener, FilterListener filterListener,
                     CrudFrameFiller crudFrameFiller, TablePanel tablePanel) throws HeadlessException {
        this.setName("CRUD Application");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.crudListener = crudListener;
        this.filterListener = filterListener;
        this.crudFrameFiller = crudFrameFiller;
        this.tablePanel = tablePanel;

        this.readButton = crudFrameFiller.getReadButton();
        this.updateButton = crudFrameFiller.getUpdateButton();
        this.createButton = crudFrameFiller.getCreateButton();
        this.removeButton = crudFrameFiller.getRemoveButton();
        this.readIdButton = crudFrameFiller.getReadIdButton();
        this.filterBox = crudFrameFiller.getFilterBox();

        setPosition();
    }

    public void run() {
        readButton.addActionListener(crudListener);
        updateButton.addActionListener(crudListener);
        createButton.addActionListener(crudListener);
        removeButton.addActionListener(crudListener);
        readIdButton.addActionListener(crudListener);
        filterBox.addItemListener(filterListener);


        tablePanel.setBounds(0, 0, 600, 600);
        readButton.setBounds(720, 10, 300, 80);
        updateButton.setBounds(720, 110, 300, 80);
        createButton.setBounds(720, 210, 300, 80);
        removeButton.setBounds(720 ,310, 300, 80);
        readIdButton.setBounds(720, 410, 300, 80);
        Container container = this.getContentPane();
        filterBox.setSize(300, 80);
        JPanel jPanel = new JPanel();
        jPanel.add(filterBox);
        jPanel.setBounds(720, 510, 300, 80);

        this.add(tablePanel);
        this.add(readButton);
        this.add(updateButton);
        this.add(createButton);
        this.add(removeButton);
        this.add(readIdButton);
        this.add(jPanel);
        this.add(new JLabel());

        this.setVisible(true);
    }

    private void setPosition() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth,
                windowHeight);
    }
}
