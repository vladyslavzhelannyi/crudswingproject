package com.crudproject.wrappers;

import redis.clients.jedis.Jedis;

public class WrapperRedis {

    private final String HOST = "localhost";
    private final int PORT = 6379;

    public Jedis getJedis() {
        return new Jedis(HOST, PORT);
    }
}

