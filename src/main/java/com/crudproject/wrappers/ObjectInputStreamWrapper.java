package com.crudproject.wrappers;

import java.io.*;

public class ObjectInputStreamWrapper {
    public ObjectInputStream getObjectInputStream(File file) throws IOException {
        return new ObjectInputStream(new FileInputStream(file));
    }
}
