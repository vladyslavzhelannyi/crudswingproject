package com.crudproject.wrappers;

import java.io.*;

public class ObjectOutputStreamWrapper {
    public ObjectOutputStream getObjectOutputStream(File file) throws IOException {
        return new ObjectOutputStream(new FileOutputStream(file));
    }
}
