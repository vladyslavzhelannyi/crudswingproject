package com.crudproject.wrappers;

import com.mongodb.MongoClient;

public class WrapperMongo {

    private final String HOST = "localhost";
    private final int PORT = 27017;

    public MongoClient getMongoClient(){
        return new MongoClient(HOST, PORT);
    }

}