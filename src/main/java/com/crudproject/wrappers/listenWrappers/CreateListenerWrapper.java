package com.crudproject.wrappers.listenWrappers;

import com.crudproject.models.jElements.jDialogs.CreatePersonDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.CreateListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.utils.supportingUtils.IdGenerator;
import com.crudproject.wrappers.fillers.CreateDialogFiller;

public class CreateListenerWrapper {
    public CreateListener getCreateListener(CreatePersonDialog createPersonDialog, CRUDService crudService,
                                            CrudValidation crudValidation, IdGenerator idGenerator,
                                            TablePanel tablePanel, CreateDialogFiller createDialogFiller) {
        return new CreateListener(createPersonDialog, crudService, crudValidation, idGenerator, tablePanel,
                createDialogFiller);
    }
}
