package com.crudproject.wrappers.listenWrappers;

import com.crudproject.models.jElements.jDialogs.DBChoiceDialog;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.ApplyListener;

import javax.swing.*;

public class ApplyListenerWrapper {
    public ApplyListener getApplyListener(CRUDService crudService, DBChoiceDialog dbChoiceDialog,
                                          JComboBox<String> dbChoiceBox){
        return new ApplyListener(crudService, dbChoiceDialog, dbChoiceBox);
    }
}
