package com.crudproject.wrappers.listenWrappers;

import com.crudproject.models.jElements.jDialogs.UpdatePersonDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.services.CRUDService;
import com.crudproject.utils.actionListeners.UpdateListener;
import com.crudproject.utils.supportingUtils.CrudValidation;
import com.crudproject.wrappers.fillers.UpdateDialogFiller;

public class UpdateListenerWrapper {
    public UpdateListener getUpdateListener(CrudValidation crudValidation, CRUDService crudService,
                                            UpdatePersonDialog updatePersonDialog, UpdateDialogFiller updateDialogFiller,
                                            TablePanel tablePanel) {
        return new UpdateListener(crudValidation, crudService, updatePersonDialog, updateDialogFiller, tablePanel);
    }
}
