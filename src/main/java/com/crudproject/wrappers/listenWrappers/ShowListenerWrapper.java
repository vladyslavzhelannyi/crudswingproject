package com.crudproject.wrappers.listenWrappers;

import com.crudproject.models.jElements.jDialogs.ReadByIdDialog;
import com.crudproject.models.jElements.jPanels.TablePanel;
import com.crudproject.utils.actionListeners.ShowListener;
import com.crudproject.utils.supportingUtils.CrudValidation;

import javax.swing.*;

public class ShowListenerWrapper {
    public ShowListener getShowListener(ReadByIdDialog readByIdDialog, CrudValidation crudValidation,
                                        TablePanel tablePanel, JTextField idField) {
        return new ShowListener(readByIdDialog, crudValidation, tablePanel, idField);
    }
}
