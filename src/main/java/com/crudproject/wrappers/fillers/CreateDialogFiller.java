package com.crudproject.wrappers.fillers;

import org.apache.jute.compiler.JField;

import javax.swing.*;

public class CreateDialogFiller {
    private JTextField nameField = new JTextField();
    private JTextField surnameField = new JTextField();
    private JTextField ageField = new JTextField();
    private JTextField cityField = new JTextField();
    private JButton createButton = new JButton("Create person");

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getSurnameField() {
        return surnameField;
    }

    public JTextField getAgeField() {
        return ageField;
    }

    public JTextField getCityField() {
        return cityField;
    }

    public JButton getCreateButton() {
        return createButton;
    }
}
