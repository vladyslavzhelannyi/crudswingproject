package com.crudproject.wrappers.fillers;

import javax.swing.*;

import static com.crudproject.constants.AgeFilterConstants.*;

public class CrudFrameFiller {
    private JButton readButton = new JButton("Read data");
    private JButton updateButton = new JButton("Update record");
    private JButton createButton = new JButton("Create record");
    private JButton removeButton = new JButton("Remove record");
    private JButton readIdButton = new JButton("Read by ID");
    private JButton filterButton = new JButton("Filter by age");
    private JComboBox<String> filterBox =
            new JComboBox<>(new String[] {NO_FILTER, FILTER018, FILTER1935, FILTER3655, FILTER56});

    public JButton getReadButton() {
        return readButton;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public JButton getCreateButton() {
        return createButton;
    }

    public JButton getRemoveButton() {
        return removeButton;
    }

    public JButton getReadIdButton() {
        return readIdButton;
    }

    public JButton getFilterButton() {
        return filterButton;
    }

    public JComboBox<String> getFilterBox() {
        return filterBox;
    }
}
