package com.crudproject.wrappers.fillers;

import javax.swing.*;

public class ReadByIdDialogFiller {
    private JTextField idField = new JTextField();
    private JButton showButton = new JButton("Show record");

    public JTextField getIdField() {
        return idField;
    }

    public JButton getShowButton() {
        return showButton;
    }
}
