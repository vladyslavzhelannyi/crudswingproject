package com.crudproject.wrappers.fillers;

import javax.swing.*;

import static com.crudproject.constants.DBConstants.*;
import static com.crudproject.constants.DBConstants.YAML_S;

public class DBChoiceDialogFiller {
    private JComboBox<String> dbChoiceBox = new JComboBox<>(new String[] {CASSANDRA_S, GRAPH_DB_S, H2_S, MONGO_DB_S,
            MY_SQL_S, POSTGRE_SQL_S, REDIS_S, BINARY_S, CSV_S, JSON_S, XML_S, YAML_S});
    private JButton dbApplyButton = new JButton("Считать данные");

    public JComboBox<String> getDbChoiceBox() {
        return dbChoiceBox;
    }

    public JButton getDbApplyButton() {
        return dbApplyButton;
    }
}
