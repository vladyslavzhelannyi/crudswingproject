package com.crudproject.wrappers.fillers;

import javax.swing.*;

public class UpdateDialogFiller {
    private JTextField nameField = new JTextField();
    private JTextField surnameField = new JTextField();
    private JTextField ageField = new JTextField();
    private JTextField cityField = new JTextField();
    private JButton updateButton = new JButton("Update person");

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getSurnameField() {
        return surnameField;
    }

    public JTextField getAgeField() {
        return ageField;
    }

    public JTextField getCityField() {
        return cityField;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }
}
