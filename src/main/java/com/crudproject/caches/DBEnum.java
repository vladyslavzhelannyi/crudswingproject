package com.crudproject.caches;

public enum DBEnum {
    JSON, XML, YAML, CSV, BINARY, MYSQL, POSTGRESQL, H2, MONGODB, REDIS, CASSANDRA, GRAPHDB
}
