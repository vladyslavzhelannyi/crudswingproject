![](img.png)
### СRUD Приложение для работы с базами данных 
____
***Приложение для работы с базами данных*** - позволяет обрабатывать список людей в файлах, 5 различных форматов, а так же в 7 базах данных. Создан в рамках обучения [DevEducation](https://deveducation.com/en/) - Base Java 2022. Ukraine.

#### Установка
Загрузить дерево проекта. Открыть его в среде разработки.

#### Предпосылки
Все необходимые maven зависимости были включены в раздачу.

#### Список команд
- Create record - модальное окно для создание новой записи
- Read data - модальное окно для выбора источника данных file или data base 
- Read by Id - считываем персону по id
- Update record - модальное окно для редактирования выбранной записи
- Remove record - выбранная запись удаляется 
- Table filter - фильтр персон по возрасту 
- Exit - выход из приложения

#### Создатели
[@vladyslavzhelannyi](https://bitbucket.org/vladyslavzhelannyi/)
[@alonadrobot](https://bitbucket.org/alonadrobot/)
[@maximbiba](https://bitbucket.org/maximbiba/)
